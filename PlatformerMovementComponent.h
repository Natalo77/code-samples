#pragma once

#pragma region Base Includes

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"

#pragma endregion

#pragma region Generated Includes

#include "PlatformerMovementComponent.generated.h"

#pragma endregion

#pragma region Forward Declarations

class UGrappleHookComp;
class UGrapplePointComp;
class APlatformerCharacter;

#pragma endregion

#pragma region UEnums

UENUM(BlueprintType)
enum class ECustomMovementMode : uint8
{
	CMM_Swinging UMETA(DisplayName = "Swinging")
};

#pragma endregion

UCLASS( Blueprintable )
class PLATFORMER_API UPlatformerMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()

#pragma region Setup

public:

	// ++ UCharacterMovementComponent Interface -- //

	/** Overrides BeginPlay from CharacterMovementComponent */
	virtual void BeginPlay() override;

	// -- UCharacterMovementComponent Interface -- //

#pragma endregion

#pragma region Update/Tick

public:
	// ++ UCharacterMovementComponent Interface ++ //

	/** Overrides TickComponent from CharacterMovementComponent */
	void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;

	// -- UCharacterMovementComponent Interface -- //

#pragma endregion

#pragma region Grapple Rope Simulation

public:

	/** Use this to calculate the spring forces at the current moment on the player (Will not check for an attachment) */
	UFUNCTION(BlueprintCallable, Category = "Grapple Rope Simulation")
		FVector CalculateSpringForces(FVector & ToPlayer, FVector & DifferenceFromRestPointOut);

	/** Use this to calculate the dampener forces at the current moment on the player (Will not check for an attachment) */
	UFUNCTION(BlueprintCallable, Category = "Grapple Rope Simulation")
		FVector CalculateDampenerForces(FVector & ToGrapple);

	// ++ UCharacterMovementComponent Interface ++ //

	/** Overrides Do Jump from Character movement */
	virtual bool DoJump(bool bReplayingMoves) override;

	// -- UCharacterMovementComponent Interface -- //

	/** Use this to give the player a boost when they jump off a grapple. */
	UFUNCTION(BlueprintCallable, Category = "Grapple Rope Simulation")
		void GivePlayerBoostFromGrappleJump(float ImpulseScaleMultiplier);

	/** Use this to inform the movement component that the player has changed the length of the rope. */
	UFUNCTION(BlueprintCallable, Category = "Grapple Rope Simulation")
		void InformComponentPlayerChangedLength();

	/** Use this to update the stored value of the equillibrium rest point */
	UFUNCTION(BlueprintCallable, Category = "Grapple Rope Simulation")
		void CalculateEquillibriumRestPoint();

	/** Use this function to rotate the player to the correct orientation when swinging on the rope. */
	UFUNCTION(BlueprintCallable, Category = "Grapple Rope Simulation")
		void RotatePlayerToGrapple();

	/** This function is used by TickComponent to perform lerps of rotation values to rotate the player. */
	UFUNCTION(BlueprintCallable, Category = "Grapple Rope Simulation")
		void PerformRotationLerps(float DeltaTime);

	/** This function is called when releasing the rope to have the player return back to their default rotation */
	UFUNCTION(BlueprintCallable, Category = "Grapple Rope Simulation")
		void RotatePlayerBackToDefault();

	/** This function is used by TickComponent to perform lerps of rotation values to rotate the player back to their default rotations. */
	UFUNCTION(BlueprintCallable, Category = "Grapple Rope Simulation")
		void PerformRotationBackToDefaultLerps(float DeltaTime);

	/** Use to set m_HorizontalReleaseForceScale */
	UFUNCTION(BlueprintCallable, Category = "Grapple Rope Simulation")
		void SetHorizontalReleaseForceScale(float scale);

protected:

	// UCharacterMovementComponent Interface //

	/** Overrides PhysFalling from CMC */
	virtual void PhysFalling(float deltaTime, int32 Iterations) override;
	// UCharacterMovementComponent Interface //

private:

	/** True if player is at or past m_TaughtDistance - Pushes remainder to remainderOut */
	bool PlayerIsAtOrPastTaughtLength(float * remainderOut);

	//==========================================================================================================================================================

	/** Used to scale HorizontalImpulseAdd in GivePlayerBoostFromGrappleJump */
	float m_HorizontalReleaseForceScale = 0.f;

	//==========================================================================================================================================================

public:

	bool m_bTempToggledTaughtOff = false;

	/** this deadzone is used when adjusting the player's position manually because of a manual rope length change. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Rope Simulation", meta = (DisplayName = "Position Adjustment Deadzone"))
		float m_PositionAdjustmentDeadzone = 5.f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Rope Simulation")
		float m_ForceApplyRemainderLengthOffset = 10.f;

	/** The spring force constant used in the spring simulation of the player. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Rope Simulation", meta = (CPF_AdvancedDisplay, DisplayName = "Spring Force Constant"))
		float m_SpringForceConstant = 8000.f;

	/** The dampening force used in the spring simulation of the player. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Rope Simulation", meta = (CPF_AdvancedDisplay, DisplayName = "Dampener Force Constant"))
		float m_DampForceConstant = 1000.f;

	/** Used to store whether the player has already had a spring force applied for the grapple point */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Rope Simulation", meta = (DisplayName = "Has Applied Grapple Force"))
		bool m_bHasAppliedGrappleForce = false;

	/** Used to determine when to start applying the grapple force. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Rope Simulation", meta = (DisplayName = "Start Applying Grapple Force"))
		bool m_bStartApplyingGrappleForce = false;

	/** The XY distance that will be used to determine how far away from the grapple to apply force. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Rope Simulation", meta = (CPF_AdvancedDisplay, DisplayName = "Horizontal Distance Force Deadzone"))
		float m_HorizontalDistanceGrappleForceBeginDeadzone = 500.f;

	/** The distance from the rest point of the system at which the spring simulation will begin to apply force. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Rope Simulation", meta = (CPF_AdvancedDisplay, DisplayName = "Distance From Rest Point To Begin Force"))
		float m_DistanceFromRestPointToBeginGrappleForce = 100.f;

	/** Storage for the current rest point of the current spring simulation */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Rope Simulation", meta = (AdvancedDisplay, DisplayName = "Position of Rest Point"))
		FVector m_CurrentSimulationRestPoint = FVector::ZeroVector;

	/** Used as storage for the vector to the equillibrium rest point of the spring simulation. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Rope Simulation", meta = (AdvancedDisplay, DisplayName = "Vector To Equillibrium Rest Point"))
		FVector m_VectorToEquillibriumRestPoint = FVector::ZeroVector;

	/** Used to inform the system whether a rotation of the capsule component is desired or not. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Rope Simulation", meta = (AdvancedDisplay, DisplayName = "Wants Rotation"))
		bool m_bWantsRotation = false;

	/** Used to represent the desired forward vector for the player when in a grapple rope simulation */
	UPROPERTY(BlueprintReadOnly, Category = "Grapple Rope Simulation", meta = (AdvancedDisplay, DisplayName = "Desired Fwd Vector for Rotation"))
		FVector m_DesiredFwdVec = FVector::ForwardVector;

	/** Used to inform the system that the player should be rotated back to their default rotation. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Rope Simulation", meta = (CPF_AdvancedDisplay, DisplayName = "Wants Default Rotation"))
		bool m_bWantsRotationDefault = false;

#pragma endregion

#pragma region Event Handlers

public:

	/** Receives and handles the event called when the grapple ropes auto length change state is changed. */
	UFUNCTION(BlueprintCallable, Category = "Grapple Event Handlers")
		void ReceiveAutoLengthChangeStateUpdate(bool NewStateOfAutoLengthChange);

#pragma endregion

#pragma region State Variables

public:

	/** Use this to return whether or not the player is below the grapple point or not. */
	UFUNCTION(BlueprintCallable, Category = "Grapple State Queries")
		bool IsPlayerBelowGrapplePoint();

	/** Use this to return whether or not the player is almost directly above the grapple point or not. */
	UFUNCTION(BlueprintCallable, Category = "Grapple State Queries")
		bool IsPlayerAlmostDirectlyAboveGrapplePoint(bool bPlayerIsBelowGrapplePoint, float XYDistFromGrapplePointSq);

	UFUNCTION(BlueprintCallable, Category = "Grapple State Queries")
		bool IsPlayerAlmostDirectlyBelowGrapplePoint(bool bPlayerIsBelowGrapplePoint, float XYDistFromGrapplePointSq, float Deadzone);

	/** Use this to return whether or not the player is within the low bound of the rope length relative to the grapple point */
	UFUNCTION(BlueprintCallable, Category = "Grapple State Queries")
		bool IsPlayerWithinLowBoundOfRope(const FVector & ToGrapple);

	/** Use this to return whether or not the player is close to the low side rest point using the provided Difference from rest point. */
	UFUNCTION(BlueprintCallable, Category = "Grapple State Queries")
		bool IsPlayerCloseToLowSideRestPoint(const FVector & DifferenceFromRestPoint, const bool bPlayerIsBelowGrapplePoint);

	/** Use this to force the movement controller to update its definition of the taught distance. */
	UFUNCTION(BlueprintCallable, Category = "Grapple State Queries")
		void UpdateTaughtDistance();

	/** Use this to ask the movement component to update its stored Grapple Point Pos. */
	UFUNCTION(BlueprintCallable, Category = "Grapple State Queries")
		void UpdateGrapplePointPos();

	/** Use this to force the system to update its stored attached to point. */
	UFUNCTION(BlueprintCallable, Category = "Grapple State Queries")
		void UpdateAttachedToPoint();

	//=========================================================================================================================================

	/** Exposes the taught distance to Bp */
	UPROPERTY(BlueprintReadOnly, Category = "Grapple State Variables", meta = (DisplayName = "Taught Distance"))
		float m_TaughtDistance = 0.0f;

	/** A state variable for whether or not the airbrake is active. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple State Variables", meta = (DisplayName = "Airbrake Active"))
		bool m_bAirbrakeActive;

	/** Storage for the player pos where the movement component is concerned. */
	UPROPERTY(BlueprintReadWrite, Category = "Grapple State Variables", meta = (DisplayName = "PlayerPos", AdvancedDisplay))
		FVector m_PlayerPos;

	/** Storage for the grapple point pos where the movement component is concerned. */
	UPROPERTY(BlueprintReadWrite, Category = "Grapple State Variables", meta = (DisplayName = "GrapplePointPos"), AdvancedDisplay)
		FVector m_GrapplePointPos;

private:

	/** Whether or not to update the taught distance every frame */
	bool m_bUpdateTaughtDistanceEveryFrame = false;

#pragma endregion

#pragma region Crouch

public:
	
	/** Set value of CrouchedHalfHeight */
	UFUNCTION(BlueprintCallable, Category = "Crouch")
		void SetCrouchHalfHeight(float value);

#pragma endregion

#pragma region Utility Functions

private:

		FVector GetVectorToPoint();
		FVector GetVectorToPlayer();

#pragma endregion

#pragma region Owner/Other class references.

public:

	/** Owner Character Pointer storage */
	UPROPERTY(BlueprintReadOnly, Category = "Grapple Owner/Other Class References", meta = (DisplayName = "Owner Character"))
		APlatformerCharacter* m_OwnerCharacter_Ref;

	/** Internal storage for a reference to the grapple hook component on the player */
	UPROPERTY(BlueprintReadOnly, Category = "Grapple Owner/Other Class References", meta = (DisplayName = "Grapple Hook Comp"))
		UGrappleHookComp* m_OwnerGrappleHook_Ref;

	/** Internal storage for a reference to the attached to grapple point */
	UPROPERTY(BlueprintReadOnly, Category = "Grapple Owner/Other Class References", meta = (DisplayName = "Grapple Point"))
		UGrapplePointComp* m_AttachedToPoint_Ref;

#pragma endregion

#pragma region Debug

private:
		
	bool DebugMessages = true;
	bool DebugDraw = true;
	bool DebugConstantMsg = true;

#pragma endregion
	
};
