#pragma once
//Class Header Comment block.
/*C+C+++C+++C+++C+++C+++C+++C+++C+++C+++C+++C+++C+++C+++C+++C+++C+++C
  Class:    UGrappleAbilityComponent

  Summary:  Provides the grapple ability to this component's actor.

  Methods:
			UGrappleAbilityComponent()
				Constructor.
			BeginPlay()
				overridden
				called when play starts
C---C---C---C---C---C---C---C---C---C---C---C---C---C---C---C---C-C*/

#pragma region Base Includes

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#pragma endregion

#pragma region Engine Includes

#include <Containers/EnumAsByte.h>
#include <Kismet/KismetSystemLibrary.h>

#pragma endregion

#pragma region Generated Includes

#include "GrappleAbilityComponent.generated.h"

#pragma endregion

#pragma region Forward Declarations

class UGrappleHookComp;

#pragma endregion

#pragma region UENums


#pragma endregion

//=================================================================================================

UCLASS( Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PLATFORMER_API UGrappleAbilityComponent : public UActorComponent
{
	GENERATED_BODY()

#pragma region Constructors

public:	
	/** Default constructor */
	UGrappleAbilityComponent();

#pragma endregion

#pragma region Setup

protected:
	// Begin AActor Interface //

	/** Called when play starts */
	virtual void BeginPlay() override;

	// End AActor Interface //

#pragma endregion

#pragma region Update/Tick

	// AActorComponent Interface //

	/** Called every frame */
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction) override;

	// AActorComponent Interface //

#pragma endregion

#pragma region Debug Variables

public:

	/** Use this to display debug messages */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool DebugMessages = false;

	/** Use this to display debug geometry */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool DebugDraw = false;

	/** Use this to display constant debug messages, such as those called in update or tick functions. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool DebugConstantMessages = false;

#pragma endregion

#pragma region Grapple Functions and Variables

public:

	/** Use to shoot a grapple towards the reticle on the HUD. */
	UFUNCTION(BlueprintCallable, Category = "Grapple")
		void ShootGrapple();

	/** Used to perform the logic for attaching the grapple to a static mesh with a grapple point */
	UFUNCTION(BlueprintCallable, Category = "Grapple")
		void AttachGrappleTo(UGrapplePointComp*& GrapplePoint);

	/** Used to perform the logic for attaching the grapple to a static mesh with a grapple point. */
	UFUNCTION(BlueprintCallable, Category = "Grapple", meta = (DisplayName = "BP_AttachGrappleTo"))
		void K2_AttachGrappleTo(UActorComponent * GrapplePointUncom, bool & bWasSuccessful);

	//============================================================================================================================

	/** The distance for which the grapple casting will be done to check for grapple objects. */
	UPROPERTY(EditAnywhere, Category = "Grapple", meta = (ClampMin = 0.0f, UIMin = 0.0f, DisplayName = "Max Grapple Distance"))
		float m_GrappleCastDistance = 100.f;

	/** The Grapple hook component to be used. */
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Grapple")
		UGrappleHookComp* m_GrappleHook;

	/** The position of the on screen reticle in world space. */
	UPROPERTY(BlueprintReadWrite, Category = "Grapple", meta = (DisplayName = "Reticle Position Vector"))
		FVector m_StartPoint;

	/** The direction into the screen - converted to world space, of the on screen reticle. */
	UPROPERTY(BlueprintReadWrite, Category = "Grapple", meta = (DisplayName = "Reticle Direction Vector"))
		FVector m_StartPointDirection;

	/** Should the grapple scanning functions use the camera or the player to determine the direction? */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple", meta = (DisplayName = "Use Camera Rotation For Grapple Scan"))
		bool m_bUseCameraForGrappleScan = true;

	/** Should the left and right grapple movement be allowed or not? */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple", meta = (DisplayName = "Allow Left and Right Grapple Movement?"))
		bool m_bAllowLeftAndRightGrappleMovement = false;


#pragma endregion

#pragma region Grapple Shoot Scanning Functions

public:

	// The highest difficulty Grapple Scan - A hit must be absolutely precise to attach.
	UFUNCTION(BlueprintCallable, Category = "Grapple Scan")
		void GrappleScanPrecise(FHitResult &OutHit);

	// The highest difficulty Grapple Scan - A hit must be absolutely precise to attach.
	UFUNCTION(BlueprintCallable, Category = "Grapple Scan")
		void K2_GrappleScanPrecise();

	/** The easiest form of Grapple scanning, continuously scans up and down for a grapple point. */
	UFUNCTION(BlueprintCallable, Category = "Grapple Scan")
		void GrappleScanContinuous(FHitResult &OutHit);

private:

	/** Used by scanning functions to perform the attachment logic. */
	void AttachmentLogic(FHitResult Hit);

public:

	/** Used by the grapple scanning continuous system to interpolate up and down. */
	UPROPERTY(BlueprintReadWrite, Category = "Grapple Scan", meta = (CPF_AdvancedDisplay, DisplayName = "Grapple Scan Trace Lerp"))
		float m_GrappleScanTraceLerp = 0.f;

	/** Used to calculate the half size vector of the box scans performed by the continuous scans. */
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Grapple Scan", meta = (AdvancedDisplay, DisplayName = "Grapple Half Size Scale Factor"))
		float m_GrappleContScanHalfSizeScaleFactor = 15.f;

	/** Used to store the halfsize of the box scans performed by the continuous grapple scans. */
	UPROPERTY(BlueprintReadOnly, Category = "Grapple Scan", meta = (AdvancedDisplay, DisplayName = "Grapple Cont Scan Half Size"))
		FVector m_GrappleContScanHalfSize;

	/** Determines what type of debug draw is used for the continuous grapple scanning */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Scan", meta = (AdvancedDisplay, DisplayName = "Grapple Cont Scan Debug Draw Type"))
	TEnumAsByte<EDrawDebugTrace::Type> m_GrappleScanDebugDraw;

	/** Used when continuously grapple scanning to decide what height (relative to the raycast) to adjust the lowest scan by */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Scan", meta = (AdvancedDisplay, DisplayName = "Grapple Cont Scan Low Height"))
		float m_GrappleContScanLowHeight = -500.f;

	/** Used when continuously grapple scanning to decide what height (relative to the raycast) to adjust the heighest scan by */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Scan", meta = (AdvancedDisplay, DisplayName = "Grapple Cont Scan High Height"))
		float m_GrappleContScanHighHeight = 3500.f;

	/** Used to decide the width of the grapple scan. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Scan", meta = (DisplayName = "Grapple Scan Width", ClampMin = "1"))
		int m_GrappleScanWidth = 1;

#pragma endregion

#pragma region Owning Actor Components.

private:

	bool m_bOwningActorHasCamera;
	class UCameraComponent* m_OwnerCamera;
	void RetrieveCamera();

	bool m_bOwningActorHasHUD;
	class APlatformerHUD* m_OwnerHUD;
	void RetrieveHUD();

	bool m_bOwningActorHasGrappleHook;
	void RetrieveGrapple();

	bool m_bOwningActorIsPlatformerCharacter;
	class APlatformerCharacter* m_OwnerCharacter;
	void RetrievePlatformerCharacter();

#pragma endregion


		
};
