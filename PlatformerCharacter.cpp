// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "PlatformerCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include <Components/SphereComponent.h>
#include <Components/SkeletalMeshComponent.h>
#include <Components/SplineComponent.h>

#include "PlatformerMovementComponent.h"



//////////////////////////////////////////////////////////////////////////
// APlatformerCharacter

void APlatformerCharacter::BeginPlay()
{
	Super::BeginPlay();

	// Set up body hitbox
	m_BodyHitbox = NewObject<USphereComponent>(Cast<UObject>(GetMesh()), USphereComponent::StaticClass(), FName(("BodyHitbox")));
	m_BodyHitbox->SetSphereRadius(150.0f);
}

bool APlatformerCharacter::IsAirbrakeActive()
{
	return m_MovementComponent_Ref->m_bAirbrakeActive;
}

void APlatformerCharacter::SetAirbrakeActive(bool bValue)
{
	m_MovementComponent_Ref->m_bAirbrakeActive = bValue;
}

bool APlatformerCharacter::IsPlayerMoving()
{
	return m_bIsMoving;
}

UPawnMovementComponent * APlatformerCharacter::GetMovementComponent() const
{
	return m_MovementComponent_Ref;
}

void APlatformerCharacter::BP_SetClimbJumpSpline(FVector InitialLocation, FVector EndLocation, bool bOffsetMidpoint, bool & bWasSuccessful)
{
	/* If Spline Component does not exist  /
	/  - Print error message			   /
	/  - Push false into bWasSuccessful	   /
	/  - Early exit						  */
	if (!m_ClimbSpline_Reference->IsValidLowLevel()) { UE_LOG(LogTemp, Error, TEXT("Please attach a BP_ClimbSpline to this level before playing...")); bWasSuccessful = false; return; }

	// Get the world location of the hand of the player, and the point to attach to.
	FVector StartLoc = InitialLocation;
	FVector EndLoc = EndLocation;

	// Calculate vectors from the start to end and start to mid.
	FVector StartToEnd = EndLoc - StartLoc;
	FVector StartToMid = StartToEnd / 2.f;

	// Calculate the world location of the midpoint.
	FVector MidPoint = StartLoc + StartToMid;

	// Create base storage for MidLoc;
	FVector MidLoc = MidPoint;

	/* If A Midpoint offset required   /
	/  - Calculate new MidLoc		  */
	if (bOffsetMidpoint) {
		// Calculate the XY relevant vector of StartToEnd.
		FVector XYToPoint = StartToEnd; XYToPoint.Z = 0;

		// Calculate the cosine angle between the Start to end and the XY relevant vector.
		float CosX = FVector::DotProduct(StartToEnd.GetSafeNormal(), XYToPoint.GetSafeNormal());
		// - use this and the length of the start to mid point to find the distance, at a normal from the toPoint vector, down to the XY relevant vector.
		float HypLength = StartToMid.Size() / CosX;
		// - use this to calculate the world location of this intersection point, and the vector from that point to the ToPoint vector midpoint.
		FVector BottomNormalPoint = StartLoc + XYToPoint.GetSafeNormal() * HypLength;
		FVector NormalVec = MidPoint - BottomNormalPoint;
		// If End location is lower and XYVec not almost zero, reverse direction of normal
		if (EndLocation.Z < InitialLocation.Z && XYToPoint.SizeSquared() <= 10.f) { NormalVec *= -1.f; }

		// - Use that and the height of the grapple throw to calculate the world location of the midpoint of the spline calculations.
		MidLoc = MidPoint + NormalVec.GetSafeNormal() * m_ClimbJumpSplineHeight;
	}

	// Set the spline component point locations.
	m_ClimbSpline_Reference->SetWorldLocation(StartLoc);
	m_ClimbSpline_Reference->SetLocationAtSplinePoint(0, StartLoc, ESplineCoordinateSpace::Type::World);
	m_ClimbSpline_Reference->SetLocationAtSplinePoint(1, MidLoc, ESplineCoordinateSpace::World);
	m_ClimbSpline_Reference->SetLocationAtSplinePoint(2, EndLoc, ESplineCoordinateSpace::World);

	// Push true out of WasSuccessful
	bWasSuccessful = true;
}

APlatformerCharacter::APlatformerCharacter(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer.SetDefaultSubobjectClass<UPlatformerMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	// Get and cast the movement component.
	m_MovementComponent_Ref = Cast<UPlatformerMovementComponent>(Super::GetMovementComponent());

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	
	// Create a camera boom (pulls in towards the player if there is a collision)
	m_CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("m_CameraBoom"));
	m_CameraBoom->SetupAttachment(RootComponent);
	m_CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	m_CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	m_FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	m_FollowCamera->SetupAttachment(m_CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	m_FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
	

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input

void APlatformerCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);

}


void APlatformerCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void APlatformerCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void APlatformerCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void APlatformerCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void APlatformerCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void APlatformerCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void APlatformerCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}
