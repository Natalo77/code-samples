#pragma region Base Includes

#include "GrappleAbilityComponent.h"

#pragma endregion

#pragma region Engine Includes

#include <Classes/Camera/CameraComponent.h>
// - UCameraComponent
#include <GameFramework/Actor.h>
// - GetOwner()
#include <Kismet/GameplayStatics.h>
// - GetPlayerController
#include <GameFramework/PlayerController.h>
// - ConvertScreenSpaceToWorld()
#include <Engine/World.h>
// - GetWorld()
#include <Components/StaticMeshComponent.h>
// - StaticMesh::StaticClass()
#include <Kismet/KismetSystemLibrary.h>
// - BoxTraceSingle
#include <Kismet/KismetMathLibrary.h>
// - FindLookAtRotation()

#pragma endregion

#pragma region Defined Includes

#include "PlatformerHUD.h"
// - APlatformerHUD
#include "GrapplePointComp.h"
// - GrapplePointComp
#include "GrappleHookComp.h"
// - GrappleHook
#include "PlatformerCharacter.h"
// - PlatformerCharacter
#include "PlatformerMovementComponent.h"
// - PlatformerMovementComponent

#pragma endregion

#pragma region Debug Includes

#include <DrawDebugHelpers.h>
// - DrawDebugLine
#include <Engine/Engine.h>
// - GEngine

#pragma endregion

//===============================================================================================================================================

/*F+F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F
  Function: UGrappleAbilityComponent

  Summary:  Default constructor.
F---F---F---F---F---F---F---F---F---F---F---F---F---F---F---F---F-F*/
UGrappleAbilityComponent::UGrappleAbilityComponent()
{
	PrimaryComponentTick.bCanEverTick = true;


	// Give an initial value to the debug draw mode for the continuous grapple scanning.
	m_GrappleScanDebugDraw = EDrawDebugTrace::None;
}

/*F+F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F
  Function: BeginPlay

  Summary:  Called when play starts
F---F---F---F---F---F---F---F---F---F---F---F---F---F---F---F---F-F*/
void UGrappleAbilityComponent::BeginPlay()
{
	Super::BeginPlay();

	// Attempt to Find the owner's camera, HUD and Grapple Hook Component.
	RetrieveCamera();
	RetrieveHUD();
	RetrieveGrapple();
	RetrievePlatformerCharacter();

	//Bind the grapple input to ShootGrapple.
	//GetOwner()->InputComponent->BindAction("Grapple", IE_Pressed, this, &UGrappleAbilityComponent::ShootGrapple);

	// Set up the half size vector using the scale size given.
	m_GrappleContScanHalfSize = { m_GrappleContScanHalfSizeScaleFactor, m_GrappleContScanHalfSizeScaleFactor, m_GrappleContScanHalfSizeScaleFactor };

	// Convert the Grapple point collision channel to a trace query.
	//m_GrapplePointTraceQuery = UEngineTypes::ConvertToTraceType(ECollisionChannel::ECC_GameTraceChannel5);
}

void UGrappleAbilityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	// Turn the crosshair position within ownerHUD into a world space co-ordinate.
	if (m_bOwningActorHasHUD) {
		UGameplayStatics::GetPlayerController(GetWorld(), 0)->DeprojectScreenPositionToWorld(
			m_OwnerHUD->m_CrosshairPosition.X,
			m_OwnerHUD->m_CrosshairPosition.Y,
			m_StartPoint,
			m_StartPointDirection);
	}
}

/*F+F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F
  Function: ShootGrapple

  Summary:  Raycasts towards the reticle on the HUD.
F---F---F---F---F---F---F---F---F---F---F---F---F---F---F---F---F-F*/
void UGrappleAbilityComponent::ShootGrapple()
{

	if (m_GrappleHook->IsAttached()) {
		m_GrappleHook->ReleaseGrapple();
		return;
	}

	// Perform the Grapple scan.
	//K2_GrappleScanPrecise();
}

void UGrappleAbilityComponent::AttachGrappleTo(UGrapplePointComp*& GrapplePoint)
{
#pragma region Debug
	if (DebugMessages) {
		if (!GrapplePoint) {
			GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Green, FString("GrappleAbilityComponent 144: Grapple point is NULL"));
			return;
		}
		if (!m_GrappleHook)
		{
			GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Green, FString("GrappleAbilityComponent 148: m_GrappleHook is NULL"));
			return;
		}
	}
#pragma endregion

	// Inform the GrapplePointComp to attach the member GrappleHookComp to it.
	GrapplePoint->AttachGrappleTo(m_GrappleHook);

	// Inform the member GrappleHookComp what it is attached to.
	m_GrappleHook->AttachToPoint(GrapplePoint);

	
}

void UGrappleAbilityComponent::K2_AttachGrappleTo(UActorComponent * GrapplePointUncom, bool & bWasSuccessful)
{
	// Early non-success return if unconfirmed grapple point is not valid.
	if (!GrapplePointUncom->IsValidLowLevel()) { bWasSuccessful = false; return; }

	// Cast the actor component to a grapple point
	UGrapplePointComp* GrapplePoint = Cast<UGrapplePointComp>(GrapplePointUncom->GetOwner()->GetComponentByClass(UGrapplePointComp::StaticClass()));

	// GrapplePoint must be valid for function to be successful
	if (GrapplePoint->IsValidLowLevel()) {

		// Push out a successful action
		bWasSuccessful = true;

		// Inform the GrapplePointComp to attach the member GrappleHookComp to it.
		GrapplePoint->AttachGrappleTo(m_GrappleHook);

		// Inform the member GrappleHookComp what it is attached to.
		m_GrappleHook->AttachToPoint(GrapplePoint);
	}
	else { bWasSuccessful = false; return; }
}

void UGrappleAbilityComponent::GrappleScanPrecise(FHitResult & OutHit)
{
	GetWorld()->LineTraceSingleByChannel(OutHit, m_StartPoint, m_StartPoint + m_StartPointDirection * m_GrappleCastDistance, ECollisionChannel::ECC_GameTraceChannel5);
}

void UGrappleAbilityComponent::K2_GrappleScanPrecise()
{
	// Storage for the line trace.
	FHitResult Hit;

	// Perform a line trace.
	GetWorld()->LineTraceSingleByChannel(Hit, m_StartPoint, m_StartPoint + m_StartPointDirection * m_GrappleCastDistance, ECollisionChannel::ECC_GameTraceChannel5);

	// Perform hit logic
	if (Hit.IsValidBlockingHit()) {
		AttachmentLogic(Hit);
	}
}

void UGrappleAbilityComponent::GrappleScanContinuous(FHitResult & OutHit)
{
	// Only scan if the player is not attached and their airbrake is not active
	/*
	if (!m_GrappleHook->IsAttached() && !m_OwnerCharacter->IsAirbrakeActive()) {

		// Increment the trace lerp value.	-	Then wrap the Trace lerp value back around to 0.
		m_GrappleScanTraceLerp += 0.1f;			if (m_GrappleScanTraceLerp > 1.f) { m_GrappleScanTraceLerp = 0.f; }

		// Calculate the proposed end point of the grapple.
		FVector EndPoint = m_StartPoint + m_StartPointDirection * m_GrappleCastDistance;
		float zOffset = FMath::Lerp(m_GrappleContScanHighHeight, m_GrappleContScanLowHeight, m_GrappleScanTraceLerp);
		EndPoint.Z += zOffset;

		// Perform the box traces.
		UKismetSystemLibrary::BoxTraceSingle(GetWorld(),
			GetOwner()->GetActorLocation(), EndPoint, m_GrappleContScanHalfSize, FRotator::ZeroRotator, 
			UEngineTypes::ConvertToTraceType(ECollisionChannel::ECC_GameTraceChannel5),
			false, {}, m_GrappleScanDebugDraw, OutHit, true);
	}
	*/

	// Width must be modified through function so copy into local value.
	int width = m_GrappleScanWidth;

	//ver 2
	if (!m_GrappleHook->IsAttached() && !m_OwnerCharacter->IsAirbrakeActive()) {

		// Increment the trace lerp value.	-	Then wrap the Trace lerp value back around to 0.
		m_GrappleScanTraceLerp += 0.1f;			if (m_GrappleScanTraceLerp > 1.1f) { m_GrappleScanTraceLerp = 0.f; }

		// Calculate the start point of the scan.
		//FVector StartScanPoint = m_StartPoint + m_StartPointDirection * m_GrappleCastDistance;
		//FVector ToStartPoint = StartScanPoint - m_StartPoint;

		// Decide whether to use the camera's rotation or the player's rotation
		FVector InitialDirection;
		if (m_bUseCameraForGrappleScan) { InitialDirection = m_OwnerCamera->GetUpVector(); }
		else { InitialDirection = m_OwnerCharacter->GetRootComponent()->GetUpVector(); }
		FVector RightVector;
		if (m_bUseCameraForGrappleScan) { RightVector = m_OwnerCamera->GetRightVector(); }
		else { RightVector = m_OwnerCharacter->GetRootComponent()->GetRightVector(); }

		// Calculate the end point of the scan.
		FVector EndScanPoint = m_OwnerCharacter->GetActorLocation() + InitialDirection * m_GrappleCastDistance;
		FVector ToEndPoint = EndScanPoint - m_OwnerCharacter->GetActorLocation();

		// Rotate the vector.
		ToEndPoint = ToEndPoint.RotateAngleAxis(m_GrappleScanTraceLerp * 90.f, RightVector);

		// Calculate the new end scan point.
		EndScanPoint = m_OwnerCharacter->GetActorLocation() + ToEndPoint;

		// Perform the box traces.
		// - If Width is odd, put one trace above the player, and adjust the remaining to the left and right.
		// - If Width is even, adjust all traces to the left and right of the player.
		if (width % 2 == 1)
		{
			// Perform Middle of Player trace.
			UKismetSystemLibrary::BoxTraceSingle(GetWorld(),
				GetOwner()->GetActorLocation(), EndScanPoint, m_GrappleContScanHalfSize, FRotator::ZeroRotator,
				UEngineTypes::ConvertToTraceType(ECollisionChannel::ECC_GameTraceChannel5),
				false, {}, m_GrappleScanDebugDraw, OutHit, true);

			// Return if a hit so future traces don't clobber data.
			if (OutHit.bBlockingHit) { return; }

			// Push the remaining traces out to the left and right.
			width--;
			width /= 2;
			for (int currentWidthPass = 1; currentWidthPass <= width; currentWidthPass++)
			{
				// Calculate Adjustment Vector.
				FVector Adjustment = GetOwner()->GetActorRightVector() * m_GrappleContScanHalfSize * 2.f * currentWidthPass;

				// Right hand side traces.
				UKismetSystemLibrary::BoxTraceSingle(GetWorld(),
					GetOwner()->GetActorLocation() + Adjustment, EndScanPoint + Adjustment, m_GrappleContScanHalfSize, FRotator::ZeroRotator,
					UEngineTypes::ConvertToTraceType(ECollisionChannel::ECC_GameTraceChannel5),
					false, {}, m_GrappleScanDebugDraw, OutHit, true);

				// Return hit 
				if (OutHit.bBlockingHit) { return; }

				// Left Hand Side Traces.
				UKismetSystemLibrary::BoxTraceSingle(GetWorld(),
					GetOwner()->GetActorLocation() - Adjustment, EndScanPoint - Adjustment, m_GrappleContScanHalfSize, FRotator::ZeroRotator,
					UEngineTypes::ConvertToTraceType(ECollisionChannel::ECC_GameTraceChannel5),
					false, {}, m_GrappleScanDebugDraw, OutHit, true);

				// Return hit.
				if (OutHit.bBlockingHit) { return; }
			}
		}
		else
		{
			// Push traces to the left and right.
			width /= 2;
			for (int currentWidthPass = 0; currentWidthPass < width; currentWidthPass++)
			{
				// Calculate adjustment vector.
				FVector Adjustment = GetOwner()->GetActorRightVector() * m_GrappleContScanHalfSize  * (1 + 2 * currentWidthPass);

				// Right hand traces.
				UKismetSystemLibrary::BoxTraceSingle(GetWorld(),
					GetOwner()->GetActorLocation() + Adjustment, EndScanPoint + Adjustment, m_GrappleContScanHalfSize, FRotator::ZeroRotator,
					UEngineTypes::ConvertToTraceType(ECollisionChannel::ECC_GameTraceChannel5),
					false, {}, m_GrappleScanDebugDraw, OutHit, true);

				// Return hit.
				if (OutHit.bBlockingHit) { return; }

				// Left hand traces.
				UKismetSystemLibrary::BoxTraceSingle(GetWorld(),
					GetOwner()->GetActorLocation() - Adjustment, EndScanPoint - Adjustment, m_GrappleContScanHalfSize, FRotator::ZeroRotator,
					UEngineTypes::ConvertToTraceType(ECollisionChannel::ECC_GameTraceChannel5),
					false, {}, m_GrappleScanDebugDraw, OutHit, true);

				// Return hit.
				if (OutHit.bBlockingHit) { return; }
			}
		}
	}
}

void UGrappleAbilityComponent::AttachmentLogic(FHitResult Hit)
{
	// Perform attachment logic
	UGrapplePointComp* hitComp = Cast<UGrapplePointComp>(Hit.GetActor()->GetComponentByClass(UGrapplePointComp::StaticClass()));
	if (hitComp->IsValidLowLevel()) {
		AttachGrappleTo(hitComp);
	}
}

/*F+F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F
  Function: RetriveCamera

  Summary:  Gets the camera from the owning actor, if there is one.

  Modifies: m_bOwningActorHasCamera
			m_OwnerCamera
F---F---F---F---F---F---F---F---F---F---F---F---F---F---F---F---F-F*/
void UGrappleAbilityComponent::RetrieveCamera()
{
	m_OwnerCamera = Cast<UCameraComponent>(GetOwner()->GetComponentByClass(UCameraComponent::StaticClass()));
	m_bOwningActorHasCamera = m_OwnerCamera->IsValidLowLevel();
	if (!m_bOwningActorHasCamera) { m_OwnerCamera = 0; }
}

/*F+F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F
  Function: RetriveHUD

  Summary:  Gets the HUD from the owning actor's player controller, if there is one.

  Modifies: m_bOwningActorHasHUD
			m_OwnerHUD
F---F---F---F---F---F---F---F---F---F---F---F---F---F---F---F---F-F*/
void UGrappleAbilityComponent::RetrieveHUD()
{
	//Does the owning actor have a PlatformerHUD
	m_OwnerHUD = Cast<APlatformerHUD>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD());
	m_bOwningActorHasHUD = m_OwnerHUD->IsValidLowLevel();
	if (!m_bOwningActorHasHUD) { m_OwnerHUD = 0; }
}

void UGrappleAbilityComponent::RetrieveGrapple()
{
	//Does the owning actor have a PlatformerHUD
	m_GrappleHook = Cast<UGrappleHookComp>(GetOwner()->GetComponentByClass(UGrappleHookComp::StaticClass()));
	m_bOwningActorHasGrappleHook = m_GrappleHook->IsValidLowLevel();
	if (!m_bOwningActorHasGrappleHook) { m_GrappleHook = 0; }
}

void UGrappleAbilityComponent::RetrievePlatformerCharacter()
{
	//Is the owning actor a platformeracharacter
	m_OwnerCharacter = Cast<APlatformerCharacter>(GetOwner());
	m_bOwningActorIsPlatformerCharacter = m_OwnerCharacter->IsValidLowLevel();
	if (!m_bOwningActorIsPlatformerCharacter) { m_OwnerCharacter = 0; }
}


