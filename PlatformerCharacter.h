// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlatformerCharacter.generated.h"

#pragma region Forward Declarations

class USphereComponent;
class UPlatformerMovementComponent;
class USplineComponent;

#pragma endregion

#pragma region UEnums

UENUM(BlueprintType)
enum class EMovementStateType : uint8
{
	MST_None				UMETA(DisplayName = "None"),
	MST_DEBUG				UMETA(DisplayName = "DEBUG"),
	MST_Walking				UMETA(DisplayName = "Walking"),
	MST_BaseJump			UMETA(DisplayName = "Base Jump"),
	MST_Glide				UMETA(DisplayName = "Glide"),
	MST_Climbing			UMETA(DisplayName = "Climbing"),
	MST_ClimbUp				UMETA(DisplayName = "ClimbUp"),
	MST_ClimbHop			UMETA(DisplayName = "ClimbHop"),
	MST_ClimbWall			UMETA(DisplayName = "Climb Wall"),
	MST_ClimbTransfer		UMETA(DisplayName = "Climb Transfer"),
	MST_ClimbWallTransfer	UMETA(DisplayName = "Climb Wall Transfer"),
	MST_Falling				UMETA(DisplayName = "Falling"),
	MST_Grapple				UMETA(DisplayName = "Grapple"),
	MST_SwimAbove			UMETA(DisplayName = "SwimAbove"),
	MST_SwimBelow			UMETA(DisplayName = "SwimBelow"),
	MST_AttackAir			UMETA(DisplayName = "Attack Air"),
	MST_AttackGround		UMETA(DisplayName = "Attack Ground"),
	MST_Airbrake			UMETA(DisplayName = "Airbrake"),
	MST_Roll				UMETA(DisplayName = "Roll"),
	MST_BounceJump			UMETA(DisplayName = "Bounce Jump"),
	MST_SteepTurn			UMETA(DisplayName = "Steep Turn"),
	MST_Crouch				UMETA(DisplayName = "Crouch"),
	MST_HighJump			UMETA(DisplayName = "High Jump"),
	MST_AttackCrouch		UMETA(DisplayName = "Attack Crouch"),
	MST_AttackDash			UMETA(DisplayName = "Attack Dash")
};

#pragma endregion

UCLASS(config=Game)
class APlatformerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* m_CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* m_FollowCamera;

	// AActor interface //

	/** Called when play starts. */
	virtual void BeginPlay() override;

	// AActor interface //

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Hitbox", meta = (DisplayName = "BodyHitbox"))
		USphereComponent* m_BodyHitbox;

	/** Storage for the movement component used by the character */
	UPROPERTY(BlueprintReadWrite, Category = "MovementComponent", meta = (DisplayName = "Movement Component"))
		UPlatformerMovementComponent* m_MovementComponent_Ref;

	/** CrouchedHalfHeight storage for MovementComponent Initialization */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "MovementComponent", meta = (DisplayName = "CrouchedHalfHeight Override"))
		float m_CrouchedHalfHeight = 0.f;

	/** Use this to return whether or not the airbrake on the platformerMovementComponent is active. */
	UFUNCTION(BlueprintCallable, Category = "Player Movement")
		bool IsAirbrakeActive();

	/** Use this to set the value of m_bAirbrakeActive within the movement component */
	UFUNCTION(BlueprintCallable, Category = "Player Movement")
		void SetAirbrakeActive(bool bValue);

	/** Storage for whether the player is moving or not. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Movement", meta = (AdvancedDisplay, DisplayName = "IsMoving"))
		bool m_bIsMoving = false;

	/** Use this to return whether the player is moving or not. */
	UFUNCTION(BlueprintCallable, Category = "Player Movement")
		bool IsPlayerMoving();

	/** Used to store the movement state of the player without affecting the character movement controller. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Movement State", meta = (DisplayName = "Movement State"))
		EMovementStateType m_MovementState = EMovementStateType::MST_Walking;

	// ACharacter interface //

	/** Returns the PlatformerMovementComponent */
	virtual UPawnMovementComponent * GetMovementComponent() const override;
	// ACharacter interface.

	/** Used to calculate and set the spline points used for moving the player between ledges */
	UFUNCTION(BlueprintCallable, Category = "Climb")
		void BP_SetClimbJumpSpline(FVector InitialLocation, FVector Endlocation, bool bOffsetMidpoint, bool & bWasSuccessful);

	/** Used to store a reference to the spline used for transferring the player between ledges */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Climb", meta = (DisplayName = "Climb Spline", CPF_AdvancedDisplay))
		USplineComponent* m_ClimbSpline_Reference = NULL;

	/** Used to temporarily store the value of the hands mid point when calculating the ClimbSpline */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Climb", meta = (DisplayName = "HandsMidPoint", CPF_AdvancedDisplay))
		FVector m_Temp_HandsMidPoint;

	/** Changes the height of the spline path when calculating climbJumpSpline */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Climb", meta = (DisplayName = "Jump Path Height"))
		float m_ClimbJumpSplineHeight = 10.f;
	

public:
	APlatformerCharacter(const class FObjectInitializer& ObjectInitializer);

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category=Camera)
	float BaseLookUpRate;

protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	/** Returns m_CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return m_CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return m_FollowCamera; }
};

