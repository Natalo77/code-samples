// Fill out your copyright notice in the Description page of Project Settings.

#pragma region Base Includes

#include "PlatformerMovementComponent.h"

#pragma endregion

#pragma region Engine Includes

#include <GameFramework/Character.h>
#include <GameFramework/Actor.h>
#include <Components/PrimitiveComponent.h>
#include <GameFramework/PhysicsVolume.h>
// - GetPhysicsVolume()
#include <Kismet/KismetMathLibrary.h>
// - FindLookAtRotation().

#pragma endregion

#pragma region Defined Includes

#include "GrappleHookComp.h"
#include "GrapplePointComp.h"
#include "PlatformerCharacter.h"

#pragma endregion

#pragma region Debug Includes

#include <DrawDebugHelpers.h>

#pragma endregion

#pragma region Stat definitions
	DECLARE_CYCLE_STAT(TEXT("Char PhysFalling"), STAT_CharPhysFalling, STATGROUP_Character);
#pragma endregion

#pragma region Magic Numbers
	const float VERTICAL_SLOPE_NORMAL_Z = 0.001f; // Slope is vertical if Abs(Normal.Z) <= this threshold. Accounts for precision problems that sometimes angle normals slightly off horizontal for vertical surface.
#pragma endregion

//==========================================================================================================================================================================================================================

#pragma region Setup

void UPlatformerMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	// Find the GrappleHookComp on the owning actor and store a reference.
	m_OwnerGrappleHook_Ref = Cast<UGrappleHookComp>(GetOwner()->GetComponentByClass(UGrappleHookComp::StaticClass()));

	// Find & Store Owning PlatformerCharacter reference, Setup CrouchedHalfHeight
	m_OwnerCharacter_Ref = Cast<APlatformerCharacter>(GetOwner());
	CrouchedHalfHeight = m_OwnerCharacter_Ref->m_CrouchedHalfHeight;

	// Bind the event receiver function to the auto length change state toggle dispatcher in grapple hook comp.
	m_OwnerGrappleHook_Ref->m_AutoLengthChangeDelegate.AddDynamic(this, &UPlatformerMovementComponent::ReceiveAutoLengthChangeStateUpdate);

	
}

#pragma endregion


#pragma region Update/Tick

void UPlatformerMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// perform tick taught length update if required.
	if (m_bUpdateTaughtDistanceEveryFrame) { UpdateTaughtDistance(); }

	/* If the movement mode is falling OR they are moving on the ground AND are attached.  /
	/  - Apply forces to the player														  */
	if ((MovementMode == EMovementMode::MOVE_Falling || IsMovingOnGround()) && m_OwnerGrappleHook_Ref->IsAttached()) 
	{
		// Update the grapple point position.
		UpdateGrapplePointPos();

		// Update the player Position
		m_PlayerPos = GetOwner()->GetActorLocation();
		// Calculate vectors to the player and to the grapple
		FVector ToPlayer = GetVectorToPlayer();
		FVector ToGrapple = GetVectorToPoint();

		// Storage for the current difference from the proposed rest point.
		FVector DifferenceFromRestPoint;

		// Calculate spring forces, extracting the DifferenceFromRestPoint in the process.
		FVector SpringForce = CalculateSpringForces(ToPlayer, DifferenceFromRestPoint);

		// Calculate Dampener forces.
		FVector DamperForce = CalculateDampenerForces(ToGrapple);

		// Calculate the players XY distance square from the grapple point
		float distAway2DSq = FVector::DistSquaredXY(m_PlayerPos, m_GrapplePointPos);

		// Is the player below the grapple point
		bool bPlayerIsBelowGrapplePoint = IsPlayerBelowGrapplePoint();
		// Is the player almost directly above the grapple point
		bool bPlayerIsAlmostDirectlyAboveGrapplePoint = IsPlayerAlmostDirectlyAboveGrapplePoint(bPlayerIsBelowGrapplePoint, distAway2DSq);
		// Is the player within the rope's low bound length relative to the grapple point.
		bool bPlayerIsWithinLowBound = IsPlayerWithinLowBoundOfRope(ToGrapple);
		// Is the player close to a low side rest point
		bool bPlayerIsCloseToLowSideRestPoint = IsPlayerCloseToLowSideRestPoint(DifferenceFromRestPoint, bPlayerIsBelowGrapplePoint);
		bool bIsPlayerPastTaughtForceApplicationDistance = ToGrapple.SizeSquared() > FMath::Pow(m_TaughtDistance, 2.f);

		/* If the system has not begun to apply force yet, and the player is at a good distance to do so:  /
		/  - Instruct the system to start applying force.											      */
		if (!m_bHasAppliedGrappleForce && (bPlayerIsCloseToLowSideRestPoint || bIsPlayerPastTaughtForceApplicationDistance)) { m_bStartApplyingGrappleForce = true; }

		/* If the player is not within the low bound of the rope, and the system should apply grapple force.  /
		/  - Commence force applications																     */
		if (!bPlayerIsWithinLowBound && m_bStartApplyingGrappleForce) {

			/* If the player is not almost directly above the grapple point:								   /
			/  - Apply the spring force.																	   /
			/  - Record that the system has applied spring force for the first time this simulation cycle.     /
			/  - Ask the system to rotate the player towards the correct point.								  */
			if (!bPlayerIsAlmostDirectlyAboveGrapplePoint) { AddForce(SpringForce); m_bHasAppliedGrappleForce = true; RotatePlayerToGrapple(); }

			/* If the player is below the grapple point, and a spring force has already been applied (do not apply dampening forces before spring forces):  /
			/  - Apply the dampening force.																												   */
			if (bPlayerIsBelowGrapplePoint && m_bHasAppliedGrappleForce) { AddForce(DamperForce * -1.f); }
		}
		/* Else: The system has stopped a force application simulation, so must be instructed to check again when it is appropriate to add force.  /
		/  - Request that the system re-calculate when to apply forces																			  */
		else { m_bStartApplyingGrappleForce = false; m_bHasAppliedGrappleForce = false; }
	}

	/* If the system has been asked to perform a rotation:  /
	/  - Perform the rotations							   */
	if (m_bWantsRotation) { PerformRotationLerps(DeltaTime); }

	/* If the system has been asked to rotate the player back to their default:  /
	/  - Perform the rotation lerps												*/
	if (m_bWantsRotationDefault) { PerformRotationBackToDefaultLerps(DeltaTime); }
}

#pragma endregion


#pragma region Grapple Rope Simulation

FVector UPlatformerMovementComponent::CalculateSpringForces(FVector & ToPlayer, FVector & DifferenceFromRestPointOut)
{
	// Get the current rest length of the spring/rope.
	float RestLength = m_TaughtDistance;

	// Update the equillibrium rest point.
	CalculateEquillibriumRestPoint();

	// Calculate a point on the 'rope sphere' in the direction of the player.
	m_CurrentSimulationRestPoint = m_GrapplePointPos + ToPlayer.GetSafeNormal() * RestLength;

	// Calculate a vector from the player to this rest point. (DISPLACEMENT)
	FVector DifferenceFromRestPoint = m_CurrentSimulationRestPoint - m_PlayerPos;

	// Push the value of DifferenceFromRestPoint out
	DifferenceFromRestPointOut = DifferenceFromRestPoint;

	// Calculate the spring force using this vector to the rest point using the spring force constant (HOOKE'S LAW)
	FVector SpringForce = DifferenceFromRestPoint * m_SpringForceConstant;

	// Return this vector.
	return SpringForce;
}

FVector UPlatformerMovementComponent::CalculateDampenerForces(FVector & ToGrapple)
{
	// Calculate the scale component of velocity towards the grapple point
	float VelocityComponentTowardsGrapple = FVector::DotProduct(Velocity, ToGrapple) / ToGrapple.Size();

	// Calculate the Full vector of velocity towards the grapple point to damp against
	FVector VelocityToDamp = ToGrapple.GetSafeNormal() * VelocityComponentTowardsGrapple;

	// Calculate the damper force using the damp force constant.
	FVector DamperForce = VelocityToDamp * m_DampForceConstant;

	return DamperForce;
}

bool UPlatformerMovementComponent::DoJump(bool bReplayingMoves)
{
	/* If the owning character exists and can jump:  /
	/  - Proceed with Jump logic					*/
	if (CharacterOwner && CharacterOwner->CanJump())
	{
		/* If the player is not constrained to a plane:  /
		/  - Proceed with jump logic					*/
		if (!bConstrainToPlane || FMath::Abs(PlaneConstraintNormal.Z) != 1.f)
		{
			/* If the player is attached to a point:  /
			/  - Proceed with jump logic			 */
			if (m_OwnerGrappleHook_Ref->IsAttached()) {

				// Get the vector to the grapple point
				FVector ToGrapple = GetVectorToPoint();

				/* If the player is moving away from the grapple point:						  /
				/  - Prevent them from jumping over the max distance from the grapple point  */
				if (FVector::DotProduct(Acceleration, ToGrapple) < SMALL_NUMBER - 10.f) {

					/* If the rope is already taught:				 /
					/  - Launch the player towards the grapple hook */
					if (m_OwnerGrappleHook_Ref->m_RopeState == ERopeState::RS_Taught) {
						Velocity += ToGrapple;
					}
					/* Else:				 /
					/  - Zero the velocity  */
					else {
						Velocity = FVector::ZeroVector;
					}
				}
				/* Else: The jump should be done as normal:  /
				/  - Do normal jump logic.					*/
				else {
					Velocity.Z = FMath::Max(Velocity.Z, JumpZVelocity);
				}
			}
			/* Else: The jump should be done as normal:  /
			/  - Do normal jump logic					*/
			else {
				Velocity.Z = FMath::Max(Velocity.Z, JumpZVelocity);
			}

			// Set the movement mode back to falling.
			SetMovementMode(MOVE_Falling);
			return true;
		}
	}
	return false;
}

void UPlatformerMovementComponent::GivePlayerBoostFromGrappleJump(float MaximumBoostSize)
{
	// Calculate the size of velocity.
	float VelSize = Velocity.Size();

	// Clamp it to a reasonable maximum.
	VelSize = FMath::Min(VelSize * 100.f, MaximumBoostSize);

	// Calculate the input force.
	FVector ImpulseForce = FVector::UpVector * VelSize;

	// Calculate additional horizontal force to add.
	FVector HorizontalImpulseAdd{ Velocity.X, Velocity.Y, 0.f };
	HorizontalImpulseAdd = HorizontalImpulseAdd.GetSafeNormal() * VelSize * m_HorizontalReleaseForceScale;

	// Sum upwardImpulse & HorizontalImpulse.
	ImpulseForce += HorizontalImpulseAdd;

	// Add the impulse to the character movement component.
	this->AddImpulse(ImpulseForce);

	// Tell the system that it has not applied grapple force yet, and that it needs to recalculate when to apply it.
	m_bHasAppliedGrappleForce = false;
	m_bStartApplyingGrappleForce = false;
}

void UPlatformerMovementComponent::InformComponentPlayerChangedLength()
{
	// Calculate the Vector to the grapple point
	FVector ToPlayer = GetVectorToPoint() * -1.f;

	// Calculate the vector to where the rope says the player should now be.
	FVector RestPoint = m_OwnerGrappleHook_Ref->GetPositionOfGrapplePoint() + ToPlayer.GetSafeNormal() * m_TaughtDistance;
	FVector ToRestPoint = RestPoint - GetOwner()->GetActorLocation();

	/* If the size of the difference vector is bigger than a deadzone:  /
	/  - Adjust the player's position by Difference				       */
	if (ToRestPoint.Size() >= m_PositionAdjustmentDeadzone) {
		MoveUpdatedComponent(ToRestPoint + ToRestPoint.GetSafeNormal(), UpdatedComponent->GetComponentRotation(), true);
	}
}

void UPlatformerMovementComponent::CalculateEquillibriumRestPoint()
{
	// Calculate the equillibrium rest point using the current taught distance.
	FVector PositionOfEquillibriumRestPoint = m_OwnerGrappleHook_Ref->GetPositionOfGrapplePoint() + FVector::DownVector * m_TaughtDistance;

	// Store the equillibrium rest point relative to the current rope length
	m_VectorToEquillibriumRestPoint = PositionOfEquillibriumRestPoint - GetOwner()->GetActorLocation();
}

void UPlatformerMovementComponent::RotatePlayerToGrapple()
{
	// NOTE: Actual interpolation of rotation is done in tickComponent to make use of DeltaTime and Lerp functions.

	// Inform the system that a rotation is desired.
	m_bWantsRotation = true;
	
	// Find the vector to the point and the player's input.
	FVector ToPoint = GetVectorToPoint().GetSafeNormal();
	FVector Input = GetPendingInputVector().GetSafeNormal();

	/* If the player's input is nearly zero:  /
	/  - Use the velocity instead			 */
	if (Input.IsNearlyZero()) { Input = Velocity; }

	// Cross the Input and ToPoint together to get a right handed tangent to the sphere.
	FVector Tangent = FVector::CrossProduct(ToPoint, Input);

	// Cross the tangent and ToPoint together to point the tangent towards the bottom of the sphere (the desired forward vector)
	m_DesiredFwdVec = FVector::CrossProduct(Tangent.GetSafeNormal(), ToPoint);
}

void UPlatformerMovementComponent::PerformRotationLerps(float DeltaTime)
{
	/* If the rotation is almost complete			 /
	/  - Inform the system the rotation is complete  /
	/  - Break out of the function					*/
	if (FVector::Parallel(m_DesiredFwdVec, GetOwner()->GetActorForwardVector(), 2.f)) { m_bWantsRotation = false; return; }

	// Find the PlayerPosition and use this to find a look at rotation using the desiredForwardVector.
	FVector PlayerPos = GetOwner()->GetActorLocation();
	FRotator RotateToLookAt = UKismetMathLibrary::FindLookAtRotation(PlayerPos, PlayerPos + m_DesiredFwdVec.GetSafeNormal());

	// Get the current rotator of the player, and interpolate this value.
	FRotator PlayerRot = GetOwner()->GetActorRotation();
	FRotator LerpRotation = FMath::RInterpTo(PlayerRot, RotateToLookAt, DeltaTime, 3.5f);

	// Set the rotation of the player to the interpolated value.
	UpdatedComponent->SetWorldRotation(LerpRotation.Quaternion());
}

void UPlatformerMovementComponent::RotatePlayerBackToDefault()
{
	m_bWantsRotationDefault = true;
}

void UPlatformerMovementComponent::PerformRotationBackToDefaultLerps(float DeltaTime)
{
	FRotator CurrentRotation = GetOwner()->GetActorRotation();
	float Roll = CurrentRotation.Roll;
	float Pitch = CurrentRotation.Pitch;

	if (FMath::Abs(Roll) < KINDA_SMALL_NUMBER && FMath::Abs(Pitch) < KINDA_SMALL_NUMBER)
	{
		m_bWantsRotationDefault = false;
		return;
	}

	Roll = FMath::FInterpTo(Roll, 0.f, DeltaTime, 3.5f);
	Pitch = FMath::FInterpTo(Pitch, 0.f, DeltaTime, 3.5f);

	FRotator NewRotation{ Pitch, CurrentRotation.Yaw, Roll };

	GetOwner()->SetActorRotation(NewRotation);
}

void UPlatformerMovementComponent::SetHorizontalReleaseForceScale(float scale)
{
	m_HorizontalReleaseForceScale = scale;
}

void UPlatformerMovementComponent::PhysFalling(float deltaTime, int32 Iterations)
{
	SCOPE_CYCLE_COUNTER(STAT_CharPhysFalling);

	if (deltaTime < MIN_TICK_TIME)
	{
		return;
	}

	FVector FallAcceleration = GetFallingLateralAcceleration(deltaTime);
	FallAcceleration.Z = 0.f;
	const bool bHasAirControl = (FallAcceleration.SizeSquared2D() > 0.f);

	float remainingTime = deltaTime;
	while ((remainingTime >= MIN_TICK_TIME) && (Iterations < MaxSimulationIterations))
	{
		Iterations++;
		const float timeTick = GetSimulationTimeStep(remainingTime, Iterations);
		remainingTime -= timeTick;

		const FVector OldLocation = UpdatedComponent->GetComponentLocation();
		const FQuat PawnRotation = UpdatedComponent->GetComponentQuat();
		bJustTeleported = false;

		RestorePreAdditiveRootMotionVelocity();

		FVector OldVelocity = Velocity;
		FVector VelocityNoAirControl = Velocity;

		// Apply input
		if (!HasAnimRootMotion() && !CurrentRootMotion.HasOverrideVelocity())
		{
			const float MaxDecel = GetMaxBrakingDeceleration();
			// Compute VelocityNoAirControl
			if (bHasAirControl)
			{
				// Find velocity *without* acceleration.
				TGuardValue<FVector> RestoreAcceleration(Acceleration, FVector::ZeroVector);
				TGuardValue<FVector> RestoreVelocity(Velocity, Velocity);
				Velocity.Z = 0.f;
				CalcVelocity(timeTick, FallingLateralFriction, false, MaxDecel);
				VelocityNoAirControl = FVector(Velocity.X, Velocity.Y, OldVelocity.Z);
			}

			// Compute Velocity
			{
				// Acceleration = FallAcceleration for CalcVelocity(), but we restore it after using it.
				TGuardValue<FVector> RestoreAcceleration(Acceleration, FallAcceleration);
				Velocity.Z = 0.f;
				CalcVelocity(timeTick, FallingLateralFriction, false, MaxDecel);
				Velocity.Z = OldVelocity.Z;
			}

			// Just copy Velocity to VelocityNoAirControl if they are the same (ie no acceleration).
			if (!bHasAirControl)
			{
				VelocityNoAirControl = Velocity;
			}
		}

		// Apply gravity
		const FVector Gravity(0.f, 0.f, GetGravityZ());
		float GravityTime = timeTick;

		// If jump is providing force, gravity may be affected.
		if (CharacterOwner->JumpForceTimeRemaining > 0.0f)
		{
			// Consume some of the force time. Only the remaining time (if any) is affected by gravity when bApplyGravityWhileJumping=false.
			const float JumpForceTime = FMath::Min(CharacterOwner->JumpForceTimeRemaining, timeTick);
			GravityTime = bApplyGravityWhileJumping ? timeTick : FMath::Max(0.0f, timeTick - JumpForceTime);

			// Update Character state
			CharacterOwner->JumpForceTimeRemaining -= JumpForceTime;
			if (CharacterOwner->JumpForceTimeRemaining <= 0.0f)
			{
				CharacterOwner->ResetJumpState();
			}
		}

		Velocity = NewFallVelocity(Velocity, Gravity, GravityTime);
		VelocityNoAirControl = bHasAirControl ? NewFallVelocity(VelocityNoAirControl, Gravity, GravityTime) : Velocity;
		const FVector AirControlAccel = (Velocity - VelocityNoAirControl) / timeTick;

		ApplyRootMotionToVelocity(timeTick);

		if (bNotifyApex && (Velocity.Z <= 0.f))
		{
			// Just passed jump apex since now going down
			bNotifyApex = false;
			NotifyJumpApex();
		}


		// Move
		FHitResult Hit(1.f);
		FVector Adjusted = 0.5f*(OldVelocity + Velocity) * timeTick;
		
		SafeMoveUpdatedComponent(Adjusted, PawnRotation, true, Hit);

		if (!HasValidData())
		{
			return;
		}

		float LastMoveTimeSlice = timeTick;
		float subTimeTickRemaining = timeTick * (1.f - Hit.Time);

		if (IsSwimming()) //just entered water
		{
			remainingTime += subTimeTickRemaining;
			StartSwimming(OldLocation, OldVelocity, timeTick, remainingTime, Iterations);
			return;
		}
		else if (Hit.bBlockingHit)
		{
			if (IsValidLandingSpot(UpdatedComponent->GetComponentLocation(), Hit))
			{
				remainingTime += subTimeTickRemaining;
				ProcessLanded(Hit, remainingTime, Iterations);
				return;
			}
			else
			{
				// Compute impact deflection based on final velocity, not integration step.
				// This allows us to compute a new velocity from the deflected vector, and ensures the full gravity effect is included in the slide result.
				Adjusted = Velocity * timeTick;

				// See if we can convert a normally invalid landing spot (based on the hit result) to a usable one.
				if (!Hit.bStartPenetrating && ShouldCheckForValidLandingSpot(timeTick, Adjusted, Hit))
				{
					const FVector PawnLocation = UpdatedComponent->GetComponentLocation();
					FFindFloorResult FloorResult;
					FindFloor(PawnLocation, FloorResult, false);
					if (FloorResult.IsWalkableFloor() && IsValidLandingSpot(PawnLocation, FloorResult.HitResult))
					{
						remainingTime += subTimeTickRemaining;
						ProcessLanded(FloorResult.HitResult, remainingTime, Iterations);
						return;
					}
				}

				HandleImpact(Hit, LastMoveTimeSlice, Adjusted);

				// If we've changed physics mode, abort.
				if (!HasValidData() || (!IsFalling() && !m_OwnerGrappleHook_Ref->IsAttached()))
				{
					return;
				}

				// Limit air control based on what we hit.
				// We moved to the impact point using air control, but may want to deflect from there based on a limited air control acceleration.
				if (bHasAirControl)
				{
					const bool bCheckLandingSpot = false; // we already checked above.
					const FVector AirControlDeltaV = LimitAirControl(LastMoveTimeSlice, AirControlAccel, Hit, bCheckLandingSpot) * LastMoveTimeSlice;
					Adjusted = (VelocityNoAirControl + AirControlDeltaV) * LastMoveTimeSlice;
				}

				const FVector OldHitNormal = Hit.Normal;
				const FVector OldHitImpactNormal = Hit.ImpactNormal;
				FVector Delta = ComputeSlideVector(Adjusted, 1.f - Hit.Time, OldHitNormal, Hit);

				// Compute velocity after deflection (only gravity component for RootMotion)
				if (subTimeTickRemaining > KINDA_SMALL_NUMBER && !bJustTeleported)
				{
					const FVector NewVelocity = (Delta / subTimeTickRemaining);
					Velocity = HasAnimRootMotion() && !CurrentRootMotion.HasOverrideVelocity() ? FVector(Velocity.X, Velocity.Y, NewVelocity.Z) : NewVelocity;
				}

				if (subTimeTickRemaining > KINDA_SMALL_NUMBER && (Delta | Adjusted) > 0.f)
				{
					// Move in deflected direction.
					SafeMoveUpdatedComponent(Delta, PawnRotation, true, Hit);

					if (Hit.bBlockingHit)
					{
						// hit second wall
						LastMoveTimeSlice = subTimeTickRemaining;
						subTimeTickRemaining = subTimeTickRemaining * (1.f - Hit.Time);

						if (IsValidLandingSpot(UpdatedComponent->GetComponentLocation(), Hit))
						{
							remainingTime += subTimeTickRemaining;
							ProcessLanded(Hit, remainingTime, Iterations);
							return;
						}

						HandleImpact(Hit, LastMoveTimeSlice, Delta);

						// If we've changed physics mode, abort.
						if (!HasValidData() || !IsFalling())
						{
							return;
						}

						// Act as if there was no air control on the last move when computing new deflection.
						if (bHasAirControl && Hit.Normal.Z > VERTICAL_SLOPE_NORMAL_Z)
						{
							const FVector LastMoveNoAirControl = VelocityNoAirControl * LastMoveTimeSlice;
							Delta = ComputeSlideVector(LastMoveNoAirControl, 1.f, OldHitNormal, Hit);
						}

						FVector PreTwoWallDelta = Delta;
						TwoWallAdjust(Delta, Hit, OldHitNormal);

						// Limit air control, but allow a slide along the second wall.
						if (bHasAirControl)
						{
							const bool bCheckLandingSpot = false; // we already checked above.
							const FVector AirControlDeltaV = LimitAirControl(subTimeTickRemaining, AirControlAccel, Hit, bCheckLandingSpot) * subTimeTickRemaining;

							// Only allow if not back in to first wall
							if (FVector::DotProduct(AirControlDeltaV, OldHitNormal) > 0.f)
							{
								Delta += (AirControlDeltaV * subTimeTickRemaining);
							}
						}

						// Compute velocity after deflection (only gravity component for RootMotion)
						if (subTimeTickRemaining > KINDA_SMALL_NUMBER && !bJustTeleported)
						{
							const FVector NewVelocity = (Delta / subTimeTickRemaining);
							Velocity = HasAnimRootMotion() && !CurrentRootMotion.HasOverrideVelocity() ? FVector(Velocity.X, Velocity.Y, NewVelocity.Z) : NewVelocity;
						}

						// bDitch=true means that pawn is straddling two slopes, neither of which he can stand on
						bool bDitch = ((OldHitImpactNormal.Z > 0.f) && (Hit.ImpactNormal.Z > 0.f) && (FMath::Abs(Delta.Z) <= KINDA_SMALL_NUMBER) && ((Hit.ImpactNormal | OldHitImpactNormal) < 0.f));
						SafeMoveUpdatedComponent(Delta, PawnRotation, true, Hit);
						if (Hit.Time == 0.f)
						{
							// if we are stuck then try to side step
							FVector SideDelta = (OldHitNormal + Hit.ImpactNormal).GetSafeNormal2D();
							if (SideDelta.IsNearlyZero())
							{
								SideDelta = FVector(OldHitNormal.Y, -OldHitNormal.X, 0).GetSafeNormal();
							}
							SafeMoveUpdatedComponent(SideDelta, PawnRotation, true, Hit);
						}

						if (bDitch || IsValidLandingSpot(UpdatedComponent->GetComponentLocation(), Hit) || Hit.Time == 0.f)
						{
							remainingTime = 0.f;
							ProcessLanded(Hit, remainingTime, Iterations);
							return;
						}
						else if (GetPerchRadiusThreshold() > 0.f && Hit.Time == 1.f && OldHitImpactNormal.Z >= GetWalkableFloorZ())
						{
							// We might be in a virtual 'ditch' within our perch radius. This is rare.
							const FVector PawnLocation = UpdatedComponent->GetComponentLocation();
							const float ZMovedDist = FMath::Abs(PawnLocation.Z - OldLocation.Z);
							const float MovedDist2DSq = (PawnLocation - OldLocation).SizeSquared2D();
							if (ZMovedDist <= 0.2f * timeTick && MovedDist2DSq <= 4.f * timeTick)
							{
								Velocity.X += 0.25f * GetMaxSpeed() * (FMath::FRand() - 0.5f);
								Velocity.Y += 0.25f * GetMaxSpeed() * (FMath::FRand() - 0.5f);
								Velocity.Z = FMath::Max<float>(JumpZVelocity * 0.25f, 1.f);
								Delta = Velocity * timeTick;
								SafeMoveUpdatedComponent(Delta, PawnRotation, true, Hit);
							}
						}
					}
				}
			}
		}

		if (Velocity.SizeSquared2D() <= KINDA_SMALL_NUMBER * 10.f)
		{
			Velocity.X = 0.f;
			Velocity.Y = 0.f;
		}
	}
}

bool UPlatformerMovementComponent::PlayerIsAtOrPastTaughtLength(float * remainderOut)
{
	// Get the size of the vector to the point.
	float length = GetVectorToPoint().Size();

	// Get the difference of this to the current taught distance.
	float remainder = length - m_TaughtDistance;

	// Calculate whether the player is past the taught distance or not.
	bool IsPlayerPastTaughtDistance = remainder >= 0;

	/* If remainderOut is specfied:								  /
	/  - Push the value of remainder out, clamped to a positive  */
	if (remainderOut) { *remainderOut = FMath::Max(0.f, remainder); }

	// Return whether or not the player is past the taught distance.
	return IsPlayerPastTaughtDistance;
}

#pragma endregion


#pragma region Event Handlers

void UPlatformerMovementComponent::ReceiveAutoLengthChangeStateUpdate(bool NewStateOfAutoLengthChange)
{
	/* If the length is being auto changed:										   /
	/  - ask the system to automatically update the taught distance every frame.  */
	if (NewStateOfAutoLengthChange) { m_bUpdateTaughtDistanceEveryFrame = true; }
	/* Else: The rope is currently locked:  /
	/  - So we can do a single update	   */
	else{ 
		m_bUpdateTaughtDistanceEveryFrame = false;
		UpdateTaughtDistance();
	}
}

#pragma endregion


#pragma region State Variables

bool UPlatformerMovementComponent::IsPlayerBelowGrapplePoint()
{
	return m_PlayerPos.Z <= m_GrapplePointPos.Z;
}

bool UPlatformerMovementComponent::IsPlayerAlmostDirectlyAboveGrapplePoint(bool bPlayerIsBelowGrapplePoint, float XYDistFromGrapplePointSq)
{
	return (!bPlayerIsBelowGrapplePoint) && (XYDistFromGrapplePointSq <= FMath::Pow(m_HorizontalDistanceGrappleForceBeginDeadzone, 2.f));
}

bool UPlatformerMovementComponent::IsPlayerAlmostDirectlyBelowGrapplePoint(bool bPlayerIsBelowGrapplePoint, float XYDistFromGrapplePointSq, float Deadzone)
{
	return (bPlayerIsBelowGrapplePoint) && (XYDistFromGrapplePointSq <= FMath::Pow(Deadzone, 2.f));
}

bool UPlatformerMovementComponent::IsPlayerWithinLowBoundOfRope(const FVector & ToGrapple)
{
	return ToGrapple.SizeSquared() <= FMath::Pow(FMath::Abs(m_OwnerGrappleHook_Ref->m_BoundOffset), 2.f);
}

bool UPlatformerMovementComponent::IsPlayerCloseToLowSideRestPoint(const FVector & DifferenceFromRestPoint, const bool bPlayerIsBelowGrapplePoint)
{
	return (DifferenceFromRestPoint.SizeSquared() <= FMath::Pow(m_DistanceFromRestPointToBeginGrappleForce, 2.f)) &&
		(bPlayerIsBelowGrapplePoint);
}

void UPlatformerMovementComponent::UpdateTaughtDistance()
{
	// Use the current maximum rope length as the taught distance.
	m_TaughtDistance = m_OwnerGrappleHook_Ref->m_CurrentMaximumRopeLength;
}

void UPlatformerMovementComponent::UpdateGrapplePointPos()
{
	// Update the stored position of the grapple point.
	m_GrapplePointPos = m_OwnerGrappleHook_Ref->GetPositionOfGrapplePoint();

	// Since this is a new grapple point, tell the system that it has not applied grapple force yet and it should not start applying grapple force yet.
	m_bHasAppliedGrappleForce = false;
	m_bStartApplyingGrappleForce = false;
}

void UPlatformerMovementComponent::UpdateAttachedToPoint()
{
	/* If the reference to the owning grapple hook comp exists:  /
	/  - Proceed with retrieving the new point					*/
	if (m_OwnerGrappleHook_Ref) {

		/* If the grapple hook comp is attached to something:							  /
		/  - Update the attached to point ref with the stored value in the grapple hook   /
		/  - Perform a one time taught distance update for the new point				 */
		if (m_OwnerGrappleHook_Ref->IsAttached()) {
			m_AttachedToPoint_Ref = m_OwnerGrappleHook_Ref->m_AttachedTo;
			UpdateTaughtDistance();
		}
	}
}

#pragma endregion


#pragma region Utility Functions

void UPlatformerMovementComponent::SetCrouchHalfHeight(float value)
{
	CrouchedHalfHeight = value;
}

FVector UPlatformerMovementComponent::GetVectorToPoint()
{
	return m_OwnerGrappleHook_Ref->GetVectorToGrapplePoint();
}

FVector UPlatformerMovementComponent::GetVectorToPlayer()
{
	return m_PlayerPos - m_GrapplePointPos;
}

#pragma endregion