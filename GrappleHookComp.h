#pragma once
//Class Header Comment block.
/*C+C+++C+++C+++C+++C+++C+++C+++C+++C+++C+++C+++C+++C+++C+++C+++C+++C
  Class:    UGrappleHookComp

  Summary:  A class to define and provide a base point for the grapple
			 hook blueprint

  Methods:
			UGrappleHookComp()
				Constructor.
			BeginPlay()
				overridden
				called when play starts
C---C---C---C---C---C---C---C---C---C---C---C---C---C---C---C---C-C*/

#pragma region Engine Includes

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#pragma endregion

#pragma region Generated Includes

#include "GrappleHookComp.generated.h"

#pragma endregion

#pragma region Forward Declarations

class UVDRopeComponent;
class UGrappleHookComp;
class UPlatformerMovementComponent;
class UForceFeedbackComponent;
class UForceFeedbackEffect;
class APlatformerCharacter;
class USplineComponent;
class USphereComponent;
class UHookEndComp;

#pragma endregion

#pragma region MC_DELEGATES

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FGrappleTo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FAutoLengthChange, bool, NewState);

#pragma endregion

#pragma region UEnums

UENUM(BlueprintType)
enum class ERopeLengthChangeType : uint8
{
	RLCT_Tightening		UMETA(DisplayName = "Smooth Interpolation"),
	RLCT_ManualChange	UMETA(DisplayName = "Linear Interpolation"),
	RLCT_None			UMETA(DisplayName = "Not changing")
};

UENUM(BlueprintType)
enum class ERopeState : uint8
{
	RS_Taught	UMETA(DisplayName = "Rope = Taught"),
	RS_Between	UMETA(DisplayName = "Rope = Between"),
	RS_Lax		UMETA(DisplayName = "Rope = Lax")
};

#pragma endregion

//===========================================================================================================================

UCLASS(Blueprintable)
class PLATFORMER_API UGrappleHookComp : public UActorComponent
{
	GENERATED_BODY()

#pragma region Constructors

public:

	/** Default Constructor */
	UGrappleHookComp();

#pragma endregion


#pragma region Setup

protected:

	// ++ AActor Interface ++ //

	/** Called when play starts */
	virtual void BeginPlay() override;

	// -- AActor Interface -- //

private:

	/** Called By BeginPlay to handle the binding of inputs */
	void BeginPlay_BindInputs();

	/** Called by BeginPlay to handle finding owner components. */
	void BeginPlay_FindOwnerComponents();

#pragma endregion


#pragma region Tick/Update

protected: 

	// ++ Begin AActor Interface ++ //

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// -- End AActor Interface -- //

private:

	float m_DeltaTime_Internal = 0.0f;

	FVector m_LastFrameActorPosition = { 0.0f, 0.0f, 0.0f };

#pragma endregion


#pragma region Debug Variables/Functions

public:

	/** Use this to display debug messages */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool DebugMessages = false;

	/** Use this to display debug geometry */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool DebugDraw = false;

	/** Use this to display constant debug messages, such as those called in update or tick functions. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool DebugConstantMessages = false;

	/** Use this to display constant debug messages relating to the taught rope application. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool DebugConstantMessagesTaughtRope = false;

	/** Use this to display constant messages relating to the swinging rope. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool DebugConstantMessagesSwingingRope = false;

	/** Use this to print the values of all the particles in the rope. */
	UFUNCTION(BlueprintCallable, Category = "Debug", meta = (DevelopmentOnly))
		void PrintParticles(UVDRopeComponent* Rope);

#pragma endregion


#pragma region Grapple Hook Storage

public:

	/** Used as C++ to blueprint storage for the GrapplePointComp this grapple hook is attached to. */
	UPROPERTY(BlueprintReadOnly, Category = "Grapple Object References", meta = (DisplayName = "Attached Point Reference"))
		UGrapplePointComp* m_AttachedTo;

	/** Used to store the location of the currently attached to grapple point */
	UPROPERTY(BlueprintReadWrite, Category = "Grapple Hook Storage", meta = (DisplayName = "Grapple Point Location"))
		FVector m_GrapplePointPos;

	/** Used as storage for the reference to the UVDRopeComponent used as the grapple rope. */
	UPROPERTY(BlueprintReadOnly, Category = "Grapple Object References", meta = (DisplayName = "Grapple Rope Reference"))
		UVDRopeComponent* m_GrappleRope_Reference = 0;

	/** Used as storage for the reference to the CharacterMovementComponent used by the player pawn */
	UPROPERTY(BlueprintReadOnly, Category = "Grapple Object References", meta = (DisplayName = "Character Movement Component Reference"))
		UPlatformerMovementComponent* m_MovementComponent_Reference;

	/** Used as storage for the reference to the Spline component used for the player. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Object References", meta = (DisplayName = "Character Spline Component Reference"))
		USplineComponent* m_SplineComponent_Reference = NULL;

	/** Used as storage for the reference to the Character that owns this object. */
	UPROPERTY(BlueprintReadOnly, Category = "Character Object References", meta = (DisplayName = "Character Reference"))
		APlatformerCharacter* m_Character_Reference = 0;

	/** Used as storage for the reference to the HookEndComp attached to the player. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Object References", meta = (CPF_AdvancedDisplay, DisplayName = "Hook End Component Reference"))
		UHookEndComp* m_HookEndComp_Reference = 0;

#pragma endregion


#pragma region Miscellaneous Grapple Hook state variables

public:

	/** Returns whether or not the rope is taught. */
	UFUNCTION(BlueprintCallable, Category = "Grapple State Checking")
		bool IsTaught();

	//===================================================================================================================

	/** Storage for the current state of the rope (lax, taught etc.) */
	UPROPERTY(BlueprintReadWrite, Category = "Grapple Hook State", meta = (DisplayName = "Current Rope State"))
		ERopeState m_RopeState;

	/** Storage for the distance to the currently attached point. */
	UPROPERTY(BlueprintReadWrite, Category = "Grapple Hook State", meta = (DisplayName = "Current Attachment Distance"))
		float m_CurrentAttachmentDistance = 0.0f;

	/** Storage for the actual length of the rope. (the visual rope length is always set to -100 of this) */
	UPROPERTY(BlueprintReadWrite, Category = "Grapple Hook State", meta = (DisplayName = "Current True Rope Length"))
		float m_CurrentRopeLength = 0.0f;

	/** Storage for whether or not the rope should be automatically tightened by the player pawn or not */
	UPROPERTY(BlueprintReadWrite, Category = "Grapple Hook State", meta = (DisplayName = "Auto Length Change"))
		bool m_bAutoLengthChange = false;

	/** The event dispatcher for whenever the state of auto length is changed. */
	UPROPERTY(BlueprintAssignable, Category = "Grapple Hook State", meta = (DisplayName = "Auto Length Change Event Dispatcher"))
		FAutoLengthChange m_AutoLengthChangeDelegate;

	/** Used to determine the initial length of the rope, and used to set it back once detatched. */
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Grapple Hook State", meta = (AdvancedDisplay, DisplayName = "Default/Initial Grapple Rope Rest Length"))
		float m_InitialRopeVisualRestLength = 100.f;

#pragma endregion


#pragma region Grapple Force Feedback

	/** Use this to change whether or not force feedback should be on for the grapple hook. */
	UFUNCTION(BlueprintCallable, Category = "Grapple Hook Force Feedback")
		void SetForceFeedbackActive(bool bForceFeedbackOn);

	/** Use this to obtain whether or not the force feedback component is currently active. */
	UFUNCTION(BlueprintCallable, Category = "Grapple Hook Force Feedback")
		bool IsForceFeedbackActive();

	//======================================================================================================================================

	/** Storage for the force feedback component on the player. */
	UPROPERTY(BlueprintReadWrite, Category = "Grapple Hook Force Feedback", meta = (DisplayName = "Force feedback component"))
		UForceFeedbackComponent* m_ForceFeedbackComponent_Reference;

	/** Used to store whether or not the force feedback component is currently playing */
	UPROPERTY(BlueprintReadWrite, Category = "Grapple Hook Force Feedback", meta = (DisplayName = "ForceFeedbackActive"))
		bool m_bForceFeedbackIsActive = false;

	/** Storage for the force feedback effect used for grappling */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Hook Force Feedback", meta = (DisplayName = "Force feedback effect"))
		UForceFeedbackEffect* m_GrappleForceFeedbackEffect;

	/** The amount the player must have moved in the last frame in order to trigger the grapple hook's force feedback effect. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Hook Force Feedback", meta = (DisplayName = "Force Feedback Movement Deadzone"), AdvancedDisplay)
		float m_ForceFeedbackMovementDeadzone = 11.f;

#pragma endregion


#pragma region Grapple Movement Events

	/** Use this function to tell the grapple hook that the player has jumped while being attached	-	will confirm the attachment if bConfirmAttachment is true. */
	UFUNCTION(BlueprintCallable, Category = "Grapple Movement Events")
		void InformPlayerJumpedWhileAttached(bool bConfirmAttachment, bool bProvideJumpForce);

	//======================================================================================================================================================================

	/** A scale multiplier for the force applied to the player when they jump while having a grapple attached. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Movement Events", meta = (DisplayName = "Grapple Jump Boost Force Scale"))
		float m_GrappleJumpBoostMaximumSize = 90000.f;

#pragma endregion


#pragma region Shoot Grapple

public:

	/** Specify a UStaticMeshComponent in this to attach the grapple point to an object. */
	UFUNCTION(BlueprintCallable, Category = "Grapple Shoot")
		void AttachToPoint(UGrapplePointComp*& Object);

	/** Use this to get the visual position for the grapple rope to attach to. */
	UFUNCTION(BlueprintCallable, Category = "Grapple Shoot")
		FVector GetPositionOfGrapplePointVisualAttachment();

	/** Use this to have the spline for the hook throwing set up its points. */
	UFUNCTION(BlueprintCallable, Category = "Grapple Shoot", meta = (DisplayName = "BP_SetSplinePoints"))
		void K2_SetSplinePoints(UActorComponent* Object, bool & bWasSuccessful);

	/** Use this to update the position of the end of the rope */
	UFUNCTION(BlueprintCallable, Category = "Grapple Shoot", meta = (DisplayName = "UpdateRopePosition"))
		void UpdateRopePosition(FVector Position, FRotator Rotation, FVector Direction);

private:

	/** Called by AttachToPoint to handle the visual aspects of the rope being attached to a point. */
	void AttachToPoint_HandleVisuals();

	/** Called by AttachToPoint to handle whatever considerations the PlatformerMovementComponent requires. */
	void AttachToPoint_HandleMovementComponent();

public:

	/** Used to setup the initial attachment to the player */
	UFUNCTION(BlueprintCallable, Category = "Grapple Shoot", meta = (DeprecatedFunction, DeprecationMessage = "Current System uses 'on-play attachment' to the player"))
		void BeginPlayRopeAttach_DEPRECATED(UVDRopeComponent* GrappleRope);

	/** Use this to edit the particle attachments to attach the rope to AttachPoint */
	UFUNCTION(BlueprintCallable, Category = "Grapple Shoot")
		void SetAttachmentPoints(AActor* AttachPoint, UVDRopeComponent* GrappleRope);

	//==========================================================================================

	/** the event dispatcher for the Grapple Hook Blueprint */
	UPROPERTY(BlueprintReadOnly, BlueprintAssignable, EditAnywhere, Category = "Grapple Shoot")
		FGrappleTo m_GrappleShootDispatcher;

	/** Used when attaching the grapple hook to a point to set the length correctly. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Shoot", meta = (ClampMax = 0.0f, UIMax = 0.0f, DisplayName = "Attachment new-length offset"))
		float m_AttachmentNewLengthOffset = -225.0f;

	/** Used to control the height of the spline generated for throwing the grapple point along. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Shoot", meta = (DisplayName = "Height of Grapple Throw"))
		float m_HeightOfGrappleThrow = 50.f;

#pragma endregion


#pragma region Release Grapple

	/** Use this function to release the grapple - will check for an attachment by itself. */
	UFUNCTION(BlueprintCallable, Category = "Release Grapple")
		void ReleaseGrapple();

	/** Use to reset the length of the rope properly. */
	UFUNCTION(BlueprintCallable, Category = "Release Grapple")
		void ReleaseAttachmentPoints();

	//===========================================================================================

	/** Given to MovementComponent : Determines horizontal force applied to player when releasing from grapple */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Release Grapple", meta = (CPF_AdvancedDisplay, DisplayName = "Horizontal Force grapple release scale"))
		float m_HorizontalGrappleReleaseScale = 2.0f;

#pragma endregion


#pragma region Taught Rope Zone Functions and Variables

public:

	/** Used to apply force to the player when moving and in the taught rope zone */
	UFUNCTION(BlueprintCallable, Category = "Grapple Taught Zone")
		void ApplyRopeTaughtForce();

	/** Use to perform a calculation of the bounds based on the maximum length. */
	UFUNCTION(BlueprintCallable, Category = "Grapple Taught Zone")
		void CalculateBoundsAndSetCurrentMaximum(float newCurrentMaximum, bool bCurrentMaxIsLowBound);

	//=======================================================================================================================================================================

	/** Used to set the current maxmium length of the rope. */
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Grapple Taught Zone", meta = (ClampMin = 250.0f, UIMin = 250.0f, DisplayName = "Current Maximum Rope Length"))
		float m_CurrentMaximumRopeLength;

	/** Used to set the maxmium length of the rope. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Taught Zone", meta = (ClampMin = 250.0f, UIMin = 250.0f, DisplayName = "Maximum Rope Length"))
		float m_OverallMaximumRopeLength = 1500.0f;

	/** NOTE: MUST BE NEGATIVE ... Use to set the low bound of the maximum rope length, relative to the maximum rope length */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Taught Zone", meta = (ClampMax = 0.0f, UIMax = 0.0f, DisplayName = "Length Bound Offset"))
		float m_BoundOffset = -250.0f;

	/** Used as storage for MaximumRopeLength + BoundOffset */
	UPROPERTY(BlueprintReadOnly, Category = "Grapple Taught Zone", meta = (DisplayName = "Low Bound Length"))
		float m_LowBoundLength;

	/** The maximum value of force to be applied to the player towards the grapple point - Should prevent outwards movement at max value. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, AdvancedDisplay, Category = "Grapple Taught Zone", meta = (ClampMin = 0.0f, UIMin = 0.0f, DisplayName = "Taught rope force application"))
		float m_TaughtRopeForceApply = 28.0f;

private:

	/** Pre-determined graph values for Taught Rope Force application for y = e^x^2 . 0 = 0-25, 1 = 26-50, 2 = 51-75, 3 = 76-100 */
		float m_TaughtRopeForce[4] = { 0.391f, 0.472f, 0.646f, 1.f };

#pragma endregion


#pragma region Rope Swinging

public:

	/** Used to apply force towards the grapple point when in the air. */
	UFUNCTION(BlueprintCallable, Category = "Grapple Swinging")
		void ApplyForceTowardsPoint();

	//==============================================================

	/** Determines the force to apply to the player when dangling from a grapple point - Should keep the player suspended. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Swinging", meta = (ClampMin = 0.0f, UIMin = 0.0f, DisplayName = "Rope Dangle Force"))
		float m_GrappleDangleForce;

	UPROPERTY(BlueprintReadOnly, Category = "Grapple Swinging", meta = (DisplayName = "Current Taught Rope length"))
		float m_CurrentTaughtLength;


#pragma endregion


#pragma region Rope Length Changing variables and functions

public:

	/** Use this to intiate the system's manual rope change */
	UFUNCTION(BlueprintCallable, Category = "Grapple Length Change")
		void BeginChangeRopeLength(float amount);

	/** Use this to initiate the system's rope tigten function */
	UFUNCTION(BlueprintCallable, Category = "Grapple Length Change")
		void BeginRopeTighten();

	/** Use this function to physically taught the rope. */
	UFUNCTION(BlueprintCallable, Category = "Grapple Length Change")
		void InterpolateRopeLength(ERopeLengthChangeType Style);

	/** Use this function to both Recalculate the rope length and update its state. - will perform auto tighten logic if bAutoTightenLogic */
	UFUNCTION(BlueprintCallable, Category = "Grapple Length Change")
		void RecalculateAndUpdateRope(bool bAutoTightenLogic);
	
	/** Use this to force an update of the current rope length. WARNING: Will not check if rope is attached to anything. - will perform auto tighten logic if bAutoTightenLogic */
	UFUNCTION(BlueprintCallable, Category = "Grapple Length Change")
		void RecalculateRopeCurrentLength(bool bAutoTightenLogic);

	/** An internal function used to set currentChangeType and bNeedsChange */
	UFUNCTION(BlueprintCallable, Category = "Grapple Length Change")
		void SetRopeLengthChangeNeededAndStyle(ERopeLengthChangeType Style, bool value);

	/** Use this function to toggle the Auto-Taught-Rope functionality */
	UFUNCTION(BlueprintCallable, Category = "Grapple Length Change")
		void ToggleAutoLengthChange();

	//=======================================================================================================================================================

	/** Use this to change the effect the RopeLength axis has on the speed at which the length is changed. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, AdvancedDisplay, Category = "Grapple Length Change", meta = (DisplayName = "Rope Length Change Scale Factor"))
		float m_ChangeRopeLengthScale = 1.0f;

	/** The amount to adjust the calculated target length of the rope when removing slack. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, AdvancedDisplay, Category = "Grapple Length Change", meta = (DisplayName = "Taught Rope Offset"))
		float m_TaughtRopeTargetLengthOffset = 0.0f;

	/** The speed at which to tighten the rope when removing slack */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, AdvancedDisplay, Category = "Grapple Length Change", meta = (DisplayName = "Taught Rope Interp Speed"))
		float m_TightenRopeInterpSpeed = 0.0f;

	/** The speed at which to change the length of the rope manually. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, AdvancedDisplay, Category = "Grapple Length Change", meta = (DisplayName = "Change Rope Length Interp Speed"))
		float m_LengthChangeInterpSpeed = 0.0f;

	/** The distance from being perfect that the per-tick taught rope interpolation will be aborted */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, AdvancedDisplay, Category = "Grapple Length Change", meta = (DisplayName = "Taught Rope Deadzone", ClampMin = 0.0f, UIMin = 0.0f))
		float m_TaughtRopeDeadzone = 0.0f;

	/** The distance from being perfec that a manual change will be aborted. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, AdvancedDisplay, Category = "Grapple Length Change", meta = (DisplayName = "Manual Change Deadzone", ClampMin = 0.0f, UIMin = 0.0f))
		float m_ManualChangeDeadzone = 0.0f;

	// Used to set a target length for the rope to adjust its length to.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, AdvancedDisplay, Category = "Grapple Length Change", meta = (DisplayName = "Rope Target Length"))
		float m_TargetLength = 0.0f;

	/** Used to indicate to the system that a change in the rope's length is desired. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, AdvancedDisplay, Category = "Grapple Length Change", meta = (DisplayName = "Rope Needs Length Change"))
		bool m_bNeedsChange = false;

	/** Used to indicate the current way in which the rope is changing length (Easing or not) */
	UPROPERTY(BlueprintReadOnly, AdvancedDisplay, Category = "Grapple Length Change", meta = (DisplayName = "Current Rope Length Change Type"))
		ERopeLengthChangeType m_CurrentChangeType = ERopeLengthChangeType::RLCT_None;

	/** This is the difference from the real length of the rope that the VICO system will use for the visuals of the rope. */
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Grapple Length Change", meta = (CPF_AdvancedDisplay, DisplayName = "Rope Rest Length Visual Offset"))
		float m_RealLengthToVisualLengthOffset = 100.f;

	/** The maximum velocity that is allowed for a grapple length decrease input */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Grapple Length Change", meta = (CPF_AdvancedDisplay, DisplayName = "Max Velocity for Grapple Length Decrease"))
		float m_MaxVelocityForLengthDecrease = 100.f;

private:

	/** calculated fro m_MaxVelocityForLengthDecrease on play. */
	float m_MaxVelocityForLengthDecreaseSq = 0.0f;


#pragma endregion


#pragma region Grapple Utility Functions

public:

	/** Use to check if the grapple hook is currently attached to anything. */
	UFUNCTION(BlueprintCallable, Category = "Grapple Utility")
		bool IsAttached();

	/** Use this to force the grapple hook rope to update its state */
	UFUNCTION(BlueprintCallable, Category = "Grapple Utility")
		void UpdateRopeState();

	/** Use this to set the grapple rope to be visible or not.  */
	UFUNCTION(BlueprintCallable, Category = "Grapple Utility")
		void SetRopeVisible(bool visible);

#pragma endregion


#pragma region Grapple Query Functions

public:
	/** Returns the direction from the player to the current grapple point. Returns zero vector if not attached or failed.*/
	UFUNCTION(BlueprintCallable, Category = "Grapple Utility")
		FVector GetVectorToGrapplePoint();

	/** Use this to get the position of the grapple point as an FVector. */
	UFUNCTION(BlueprintCallable, Category = "Grapple Utility")
		FVector GetPositionOfGrapplePoint();

#pragma endregion


};
