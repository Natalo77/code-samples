#pragma region Base Includes

#include "GrappleHookComp.h"

#pragma endregion

#pragma region Engine Includes

#include <Components/StaticMeshComponent.h>
// - StaticMesh.Getname()
#include <Components/SphereComponent.h>
// - USphereComponent
#include <GameFramework/Actor.h>
// - InputComponent.BindAction()
#include <Components/InputComponent.h>
// - InputComponent.
// - FInputActionBinding
#include <GameFramework/CharacterMovementComponent.h>
// - UCharacterMovementComponent
#include <Components/ForceFeedbackComponent.h>
// - ForceFeedbackComponent
#include <GameFramework/ForceFeedbackEffect.h>
// - UForceFeedbackEffect.
#include <Components/SplineComponent.h>
// - USplineComponent
#include <Components/SkeletalMeshComponent.h>
// - Skeletal mesh component
#include <Components/SphereComponent.h>
// - USphereComponent

#pragma endregion

#pragma region Defined Includes

#include "GrapplePointComp.h"
// - GrapplePointComp
#include "GrapplePointEditorOffsetVisual.h"
// - grapplePointEditorOffsetVisual
#include "PlatformerMovementComponent.h"
// - PlatformerMovementComponent
#include "GrapplePointEditorOffsetVisual.h"
// - GrapplePointEditorOffsetVisual.
#include "PlatformerCharacter.h"
// - APlatformerCharacter
#include "HookEndComp.h"
// - UHookEndComp

#pragma endregion

#pragma region Plugin Includes

#define VICODYNAMICS_API
#include <../Plugins/Marketplace/VICODynamicsPlugin/Source/VICODynamicsPlugin/Public/VDRopeComponent.h>

#pragma endregion

#pragma region Debug Includes

#include <Engine/Engine.h>
// - GEngine
#include <Kismet/KismetStringLibrary.h>
// - Kismet Bool to String
#include <DrawDebugHelpers.h>
// - DrawDebugLine

#pragma endregion

//======================================================================================================================================

#pragma region Constructors

/*F+F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F
  Function: UGrappleHook

  Summary:  Default constructor.

  Modifies:	m_GrappleRope.
F---F---F---F---F---F---F---F---F---F---F---F---F---F---F---F---F-F*/
UGrappleHookComp::UGrappleHookComp()
{
	// The component is allowed to tick.
	PrimaryComponentTick.bCanEverTick = true;

	// Set initial values.
	m_bAutoLengthChange = false;
	m_RopeState = ERopeState::RS_Lax;

}

#pragma endregion


#pragma region Setup

/*F+F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F+++F
  Function: BeginPlay

  Summary:  Called on start of play.
F---F---F---F---F---F---F---F---F---F---F---F---F---F---F---F---F-F*/
void UGrappleHookComp::BeginPlay()
{
	Super::BeginPlay();

	// Don't allow the component to tick until pre-requisites have been established.
	SetComponentTickEnabled(false);

	// Set an initial value for the current max rope length and Calculate the low bounds of the maximum rope length. (for movement restriction)
	CalculateBoundsAndSetCurrentMaximum(m_OverallMaximumRopeLength, false);

	// Calculate a squared value for the length decrease velocity.
	m_MaxVelocityForLengthDecreaseSq = m_MaxVelocityForLengthDecrease * m_MaxVelocityForLengthDecrease;

	// Bind inputs for the system.
	BeginPlay_BindInputs();

	// Find and store references to required owner components.
	BeginPlay_FindOwnerComponents();

	// Allow the component to now tick safely.
	SetComponentTickEnabled(true);
}


void UGrappleHookComp::BeginPlay_FindOwnerComponents()
{
	// Find the Grapple rope on the character and store the reference to it
	m_GrappleRope_Reference = Cast<UVDRopeComponent>(GetOwner()->GetComponentByClass(UVDRopeComponent::StaticClass()));
	// - Do the same with the character movement component.
	m_MovementComponent_Reference = Cast<UPlatformerMovementComponent>(GetOwner()->GetComponentByClass(UPlatformerMovementComponent::StaticClass()));
	// - Setup MovementComponent
	{
		m_MovementComponent_Reference->SetHorizontalReleaseForceScale(m_HorizontalGrappleReleaseScale);
	}
	// - Do the same with the Character.
	m_Character_Reference = Cast<APlatformerCharacter>(GetOwner());
	// - Do the same with the HookEndComp
	m_HookEndComp_Reference = Cast<UHookEndComp>(GetOwner()->GetComponentByClass(UHookEndComp::StaticClass()));
	// - Do the same with the force feedback component.
	m_ForceFeedbackComponent_Reference = Cast<UForceFeedbackComponent>(GetOwner()->GetComponentByClass(UForceFeedbackComponent::StaticClass()));
	// - Setup the ForceFeedback Component.
	{
		// Set the effect, ask it to loop, and perform a one time stop to ensure it does not start upon game begin.
		m_ForceFeedbackComponent_Reference->SetForceFeedbackEffect(m_GrappleForceFeedbackEffect);
		m_ForceFeedbackComponent_Reference->bLooping = true;
		m_ForceFeedbackComponent_Reference->Stop();
	}	

}


void UGrappleHookComp::BeginPlay_BindInputs()
{
	//Bind the RopeTighten and RopeChangeLength
	//GetOwner()->InputComponent->BindAction("TightenRope", IE_Pressed, this, &UGrappleHookComp::ToggleAutoLengthChange);
	GetOwner()->InputComponent->BindAxis("RopeLength", this, &UGrappleHookComp::BeginChangeRopeLength);

	// Set up Action Binding for jump.
	FInputActionBinding JumpActionBinding{ "Jump", IE_Pressed };
	FInputActionHandlerSignature JumpActionHandler;	JumpActionHandler.BindUFunction(this, FName("InformPlayerJumpedWhileAttached"), true);
	JumpActionBinding.ActionDelegate = JumpActionHandler;
	GetOwner()->InputComponent->AddActionBinding(JumpActionBinding);
}

#pragma endregion


#pragma region Tick/Update

void UGrappleHookComp::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	// Update the internal storage of Delta Time.
	m_DeltaTime_Internal = DeltaTime;

	/* If the player is attached to a grapple point: /	
	/  - Perform Rope state updates					 /
	/  - Perform Rope length changes				 /
	/  - Apply grounded taught forces				*/
	if (IsAttached()) {

		/* if the last frame actor position has changed:						  /
		/  - Perform Force Feedback logic if required							  /
		/  - Recalculate the rope lenth variables, performing autotighten logic.  /
		/  - Update the rope state as a result of the above.					  /
		/  - Update the storage of the last frame's actor position				 */
		if ( FMath::Abs( (m_LastFrameActorPosition - GetOwner()->GetActorLocation()).SizeSquared()) >= m_ForceFeedbackMovementDeadzone ) {
			
			/* If the force feedback is not active and the player is not moving on the ground:  /
			/  - Activate the force feedback												   */
			if (!IsForceFeedbackActive() && !m_MovementComponent_Reference->IsMovingOnGround()) { SetForceFeedbackActive(true); }
			RecalculateAndUpdateRope(true);
			m_LastFrameActorPosition = GetOwner()->GetActorLocation();
		}
		/* Else: The actor has not changed position, but the player may have still changed the rope length, so:  /
		/  - Perform Force Feedback Logic if required															 /
		/  - Update the rope state, but do not recalculate the rope or update the last frame player pos			*/
		else 
		{ 
			/* If force feedback is active:  /
			/  - Deactivate it				*/
			if (IsForceFeedbackActive()) { SetForceFeedbackActive(false); }
			UpdateRopeState(); 
		}

		// If Rope needs a length change this frame: Commence it, using the current change type.
		if (m_bNeedsChange) { InterpolateRopeLength(m_CurrentChangeType); }

		// Apply force towards the grapple point if needed (On ground taught force) 
		ApplyRopeTaughtForce();
	}
	/* Else: The player is not attached, So:  /
	/  - Perform Force feedback logic.		 */
	else {

		/* If the force feedback is currently active (the player has dropped off):  /
		/  - Deactivate the force feedback										   */
		if (IsForceFeedbackActive()) { SetForceFeedbackActive(false); }
	}
}

#pragma endregion


#pragma region Grapple Hook Force Feedback

void UGrappleHookComp::SetForceFeedbackActive(bool bForceFeedbackOn)
{
	UE_LOG(LogTemp, Warning, TEXT("SetForceFeedbackActive: %s"), bForceFeedbackOn ? TEXT("True") : TEXT("False"));

	// Update the state of force feedback.
	m_bForceFeedbackIsActive = bForceFeedbackOn;

	/* If force feedback is now active:  /
	/  - Play the effect				*/
	if (m_bForceFeedbackIsActive) {
		m_ForceFeedbackComponent_Reference->Play();
	}
	/* Else: The force feedback is now off:  /
	/  - So stop it							*/
	else{
		m_ForceFeedbackComponent_Reference->Stop();
	}
}

bool UGrappleHookComp::IsForceFeedbackActive()
{
	return m_bForceFeedbackIsActive;
}

#pragma endregion


#pragma region Debug Variables/Functions

//======================================================================
//							DEBUG FUNCTIONS
//======================================================================
void UGrappleHookComp::PrintParticles(UVDRopeComponent * Rope)
{
	int x = 0;
	for (auto a : Rope->GetParticlesArray())
	{
		AActor* temp = Cast<AActor>(a->Attachment.ComponentRef.OtherActor);
		if(temp != NULL && temp->IsValidLowLevel()) { UE_LOG(LogTemp, Warning, TEXT("%s: %s"), *(FString::FromInt(x)), *(a->Attachment.ComponentRef.OtherActor->GetName())); }
		else{ UE_LOG(LogTemp, Warning, TEXT("%s: NULL"), *(FString::FromInt(x))); }
		//UE_LOG(LogTemp, Warning, TEXT("%s: AttachParticle: %s"), *(FString::FromInt(x), a->IsAttached ? FString("True") : FString("False")));
		
		x++;
	}
}

#pragma endregion


#pragma region Miscellaneous State Functions

bool UGrappleHookComp::IsTaught()
{
	return m_RopeState == ERopeState::RS_Taught;
}

#pragma endregion


#pragma region Grapple Movement Events

void UGrappleHookComp::InformPlayerJumpedWhileAttached(bool bConfirmAttachment, bool bProvideJumpForce)
{
	// Early exit for if the player is not attached to a point, and this function was asked to confirm the attachment of the player.
	if (bConfirmAttachment && !this->IsAttached()) { return; }

	// Detach the grapple rope.
	this->ReleaseGrapple();

	// If this function was asked to give a jump force: Ask the movement component to give the player a boost, using the grapple jump boost force scale from this component.
	if (bProvideJumpForce) { m_MovementComponent_Reference->GivePlayerBoostFromGrappleJump(m_GrappleJumpBoostMaximumSize); }
}

#pragma endregion


#pragma region Shoot Grapple

void UGrappleHookComp::AttachToPoint(UGrapplePointComp *& Object)
{
	// Store the currently attached point
	m_AttachedTo = Object;

	// Store the new position of the grapple point.
	UStaticMeshComponent* TempStaticMesh = Cast<UStaticMeshComponent>(m_AttachedTo->GetOwner()->GetComponentByClass(UStaticMeshComponent::StaticClass()));
	m_GrapplePointPos = TempStaticMesh->GetSocketLocation("GrappleSocket");

	// perform rope recalculations.
	RecalculateAndUpdateRope(true);

	// Handle the visuals of attaching a rope.
	AttachToPoint_HandleVisuals();	

	// if not set to auto change length, toggle this.
	if (!m_bAutoLengthChange) { ToggleAutoLengthChange(); }

	// Handle requests to the movement component.
	AttachToPoint_HandleMovementComponent();
}


FVector UGrappleHookComp::GetPositionOfGrapplePointVisualAttachment()
{
	if (!IsAttached()) { return FVector::ZeroVector; }

	UGrapplePointEditorOffsetVisual* Temp = Cast<UGrapplePointEditorOffsetVisual>(m_AttachedTo->GetOwner()->GetComponentByClass(UGrapplePointEditorOffsetVisual::StaticClass()));

	FVector GrapplePos = GetPositionOfGrapplePoint();
	FVector FinalPos = GrapplePos + Temp->m_Offset;

	UE_LOG(LogTemp, Warning, TEXT("GrapplePos: X: %f Y: %f Z: %f"), GrapplePos.X, GrapplePos.Y, GrapplePos.Z);
	UE_LOG(LogTemp, Warning, TEXT("FinalPos: X: %f Y: %f Z: %f"), FinalPos.X, FinalPos.Y, FinalPos.Z);

	return FinalPos;
}


void UGrappleHookComp::K2_SetSplinePoints(UActorComponent * Object, bool & bWasSuccessful)
{
	/* If Spline Component does not exist  /
	/  - Print error message			   /
	/  - Push false into bWasSuccessful	   /
	/  - Early exit						  */
	if (!m_SplineComponent_Reference->IsValidLowLevel()) { UE_LOG(LogTemp, Error, TEXT("Please attach a BP_RopeSplineActor to this level before playing...")); bWasSuccessful = false; return; }

	// Get the world location of the hand of the player, and the point to attach to.
	FVector StartLoc = m_GrappleRope_Reference->GetParticlesArray()[0]->GetLocation();
	FVector EndLoc = Object->GetOwner()->GetActorLocation();

	// Calculate vectors from the start to end and start to mid.
	FVector StartToEnd = EndLoc - StartLoc;
	FVector StartToMid = StartToEnd / 2.f;

	// Calculate the world location of the midpoint.
	FVector MidPoint = StartLoc + StartToMid;

	// Calculate the XY relevant vector of StartToEnd.
	FVector XYToPoint = StartToEnd; XYToPoint.Z = 0;

	// Calculate the cosine angle between the Start to end and the XY relevant vector.
	float CosX = FVector::DotProduct(StartToEnd.GetSafeNormal(), XYToPoint.GetSafeNormal());
	// - use this and the length of the start to mid point to find the distance, at a normal from the toPoint vector, down to the XY relevant vector.
	float HypLength = StartToMid.Size() / CosX;
	// - use this to calculate the world location of this intersection point, and the vector from that point to the ToPoint vector midpoint.
	FVector BottomNormalPoint = StartLoc + XYToPoint.GetSafeNormal() * HypLength;
	FVector NormalVec = MidPoint - BottomNormalPoint;
	// - Use that and the height of the grapple throw to calculate the world location of the midpoint of the spline calculations.
	FVector MidLoc = MidPoint + NormalVec.GetSafeNormal() * m_HeightOfGrappleThrow;
	
	// Set the spline component point locations.
	m_SplineComponent_Reference->SetWorldLocation(StartLoc);
	m_SplineComponent_Reference->SetLocationAtSplinePoint(0, StartLoc, ESplineCoordinateSpace::Type::World);
	m_SplineComponent_Reference->SetLocationAtSplinePoint(1, MidLoc, ESplineCoordinateSpace::World);
	m_SplineComponent_Reference->SetLocationAtSplinePoint(2, EndLoc, ESplineCoordinateSpace::World);
	
	// Attach the end point of the rope to the sphere collider to allow the sphere to follow the spline points.
	m_GrappleRope_Reference->GetParticlesArray()[m_GrappleRope_Reference->GetParticlesArray().Num() - 1]->AttachToComponent(m_HookEndComp_Reference->GetHookEndSphereCollider(), FVector::ZeroVector);

	// Push true out of WasSuccessful
	bWasSuccessful = true;
}


void UGrappleHookComp::UpdateRopePosition(FVector Position, FRotator Rotation, FVector Direction)
{
	m_HookEndComp_Reference->GetHookEndSphereCollider()->SetWorldLocation(Position);

	Position += Direction * 45.f;
	m_HookEndComp_Reference->GetHookEndMeshComponent()->SetWorldLocation(Position);

	Rotation.Pitch -= 90.f;
	m_HookEndComp_Reference->GetHookEndMeshComponent()->SetWorldRotation(Rotation);
}


void UGrappleHookComp::AttachToPoint_HandleVisuals()
{
	// have the rope attach to the specified point.
	SetAttachmentPoints(m_AttachedTo->GetOwner(), m_GrappleRope_Reference);

	// Set the rope and hook to be visible.
	SetRopeVisible(true);

	m_HookEndComp_Reference->GetHookEndMeshComponent()->SetWorldScale3D(FVector(1.5f, 1.5f, 1.5f));
}

void UGrappleHookComp::AttachToPoint_HandleMovementComponent()
{
	// Perform a one time dispatch of the auto length change, to make sure the platformer movement component initializes the taught distance.
	m_AutoLengthChangeDelegate.Broadcast(m_bAutoLengthChange);

	// Ask the movement component to update its stored Grapple Point Pos.
	m_MovementComponent_Reference->UpdateGrapplePointPos();

	/* if the player is in the air:									  /
	/  - toggle the auto change length back off to tighten the rope. */
	if (!m_MovementComponent_Reference->IsMovingOnGround()) { ToggleAutoLengthChange(); }
}


void UGrappleHookComp::SetAttachmentPoints(AActor * AttachPoint, UVDRopeComponent* GrappleRope)
{
	// Find the location of attach point
	FVector Loc = GetPositionOfGrapplePointVisualAttachment();

	// Attach the particle to the actor and adjust its position.
	//GrappleRope->GetParticlesArray()[GrappleRope->GetParticlesArray().Num() - 1]->AttachToComponent(Cast<UStaticMeshComponent>(AttachPoint->GetComponentByClass(UStaticMeshComponent::StaticClass())), FVector::ZeroVector, false, FName("GrappleSocket"));

	// Adjust the rope settings based on distance, using the offset to transform the real length of the rope into one suitable for the visuals.
	GrappleRope->SetNewRestLength(m_CurrentAttachmentDistance - m_RealLengthToVisualLengthOffset);

	// Store the actual length of the rope.
	m_CurrentRopeLength = m_CurrentAttachmentDistance;

	// Do a one time tighten.
	BeginRopeTighten();

	// Update the initial rope state
	UpdateRopeState();
}

#pragma endregion


#pragma region Release Grapple

void UGrappleHookComp::ReleaseGrapple()
{
	// If this rope is not attached to a grapple point, exit early.
	if (!this->IsAttached()) { return; }

	// Clear the attachment.
	m_AttachedTo = NULL;

	// Reset the attachment points (state of the rope).
	this->ReleaseAttachmentPoints();

	// Set the rope and hook to be invisible.
	SetRopeVisible(false);
	m_HookEndComp_Reference->SetHookEndRopeVisible(false);

	// If set to auto length change, toggle it off.
	if (this->m_bAutoLengthChange) { ToggleAutoLengthChange(); }
}

void UGrappleHookComp::ReleaseAttachmentPoints()
{
	// TODO: Make the rope move along a spline, based on whether in the air or not.

	// Detach the rope from the original point.
	m_GrappleRope_Reference->GetParticlesArray()[m_GrappleRope_Reference->GetParticlesArray().Num() - 1]->Detach();

	// Change the rest length of the rope back to the initial/default value
	m_GrappleRope_Reference->SetNewRestLength(m_InitialRopeVisualRestLength);

	m_GrappleRope_Reference->GetParticlesArray()[m_GrappleRope_Reference->GetParticlesArray().Num() - 1]->AttachToComponent(Cast<UPrimitiveComponent>(GetOwner()->GetComponentByClass(USkeletalMeshComponent::StaticClass())), FVector::ZeroVector);
	m_GrappleRope_Reference->GetParticlesArray()[m_GrappleRope_Reference->GetParticlesArray().Num() - 1]->SetLocation(GetOwner()->GetActorLocation());
}

#pragma endregion


#pragma region Length Change

// Should not be called directly. Gets called from Tick, which uses DeltaTime to determine when this function needs to be called.
void UGrappleHookComp::InterpolateRopeLength(ERopeLengthChangeType Style)
{
	// Does this operation need to break? (The current length is within a deadzone of the target length)
	if ((m_CurrentChangeType == ERopeLengthChangeType::RLCT_Tightening && FMath::Abs(m_CurrentRopeLength - m_TargetLength) <= m_TaughtRopeDeadzone) ||
		(m_CurrentChangeType == ERopeLengthChangeType::RLCT_ManualChange && FMath::Abs(m_CurrentRopeLength - m_TargetLength) <= m_ManualChangeDeadzone)) {

		// Reset any length change storage values, do a final rope state update and break.
		SetRopeLengthChangeNeededAndStyle(ERopeLengthChangeType::RLCT_None, false);
		UpdateRopeState();
		return;
	}

	// Update the current Rope state (updates m_RopeState).
	UpdateRopeState();

	float NewLength = 0.f;
	// Set the new length of the rope using the interpolation style specified.

	// If style is None, return.
	if (Style == ERopeLengthChangeType::RLCT_None) { return; }

	// Switch style to calculate the new length.
	switch (Style){
	case ERopeLengthChangeType::RLCT_Tightening:
		NewLength = FMath::FInterpTo(m_CurrentRopeLength, m_TargetLength, m_DeltaTime_Internal, m_TightenRopeInterpSpeed);
		break;
	case ERopeLengthChangeType::RLCT_ManualChange:
		NewLength = FMath::FInterpConstantTo(m_CurrentRopeLength, m_TargetLength, m_DeltaTime_Internal, m_LengthChangeInterpSpeed);
		break;
	}

	// Update the current rope length and set the new visual length.
	m_CurrentRopeLength = NewLength;
	m_GrappleRope_Reference->SetNewRestLength(NewLength - m_RealLengthToVisualLengthOffset);
	
}


void UGrappleHookComp::BeginChangeRopeLength(float amount)
{
	// If the player is currently moving, and there is no change currently occurring, and they are trying to decrease the rope length - return.
	//if (m_Character_Reference->IsPlayerMoving() && !m_bNeedsChange && amount < -0.5f) { return; }

	bool bDecreasingLength = amount < -0.5f;
	bool bIncreasingLength = amount > 0.5f;

	// If the rope is not attached to anything, break
	if (!IsAttached()) { return; }

	// if decreasing and already at minimum length, break											&&		// If increasing and already at max length (with the highest maximum length), break
	if (bDecreasingLength && m_CurrentRopeLength <= FMath::Abs(m_BoundOffset) + 10.f) { return; }	    		if (bIncreasingLength && m_RopeState == ERopeState::RS_Lax && m_CurrentMaximumRopeLength == m_OverallMaximumRopeLength) { return; }

	// If increasing auto tighten, toggle auto taught logic.
	if (bIncreasingLength && m_bAutoLengthChange) { ToggleAutoLengthChange(); }

	// Slingshot prevention
	// If PlayerVelocity > Maximum & Decreasing : return
	if (m_MovementComponent_Reference->Velocity.SizeSquared() >= m_MaxVelocityForLengthDecreaseSq && bDecreasingLength) { return; }

	// Calculate the target length and clamp it between the preset bounds.
	//m_TargetLength += m_ChangeRopeLengthScale * amount;
	m_TargetLength = FMath::Clamp(m_TargetLength, FMath::Abs(m_BoundOffset) + 10.f, m_OverallMaximumRopeLength);
	
	/* If the target length is within the preset bounds:			 /
	/  - reset the maximums using the target length.				 /
	/  - Ask the movement component to update its taught distance.	*/
	if (m_TargetLength <= m_OverallMaximumRopeLength && m_TargetLength > FMath::Abs(m_BoundOffset) + 10.f) {
		CalculateBoundsAndSetCurrentMaximum(m_TargetLength, false);
		m_MovementComponent_Reference->UpdateTaughtDistance();

		// Allow the rope length changes to take effect.
		SetRopeLengthChangeNeededAndStyle(ERopeLengthChangeType::RLCT_ManualChange, true);
	}

	
}


void UGrappleHookComp::BeginRopeTighten()
{
	// If the rope is not attached to anything, break this operation.
	if (!IsAttached()) { return; }

	// Set the target length.
	m_TargetLength = m_CurrentAttachmentDistance;

	// Allow the system to modify the rope length.
	SetRopeLengthChangeNeededAndStyle(ERopeLengthChangeType::RLCT_Tightening, true);
}


void UGrappleHookComp::RecalculateAndUpdateRope(bool bAutoTightenLogic)
{
	RecalculateRopeCurrentLength(bAutoTightenLogic);
	UpdateRopeState();
}


void UGrappleHookComp::RecalculateRopeCurrentLength(bool bAutoTightenLogic)
{
	// Recalculate the currentattachment distance
	m_CurrentAttachmentDistance = GetVectorToGrapplePoint().Size();

	/* If this function has been asked to perform Auto tightening logic:  /
	/  - Perform said logic												 */
	if (bAutoTightenLogic) {

		/* If the CurrentAttachmentDistance is longer than the CurrentRopeLength:  /
		/  - Proceed with Auto Tighten Logic									  */
		if (m_CurrentAttachmentDistance > m_CurrentRopeLength) {

			/* If the CurrentAttachmentDistance is not greater than the CurrentMaximumRopeLength (This is a visual consideration):  /
			/  - Proceed with Auto Tighten logic.																				   */
			if (!(m_CurrentAttachmentDistance > m_CurrentMaximumRopeLength)) {

				/* If the Player is moving on the ground or they are not falling (Ensures ground taught forces are not applied when in the air.):    /
				/  - Proceed with Auto Tighten Logic																								*/
				if (m_MovementComponent_Reference->IsMovingOnGround() || !m_MovementComponent_Reference->IsFalling()) {
					
					// Auto Tighten Logic.
					BeginRopeTighten(); 
				} 
			}
		}
		/* Else: We are not moving away from the grapple point but:				 /
		/  If the system is being instructed to automatically change the length: /
		/  - Perform Tighten Logic											*/
		else if (m_bAutoLengthChange) { BeginRopeTighten(); }
	}
}


void UGrappleHookComp::SetRopeLengthChangeNeededAndStyle(ERopeLengthChangeType Style, bool value)
{
	// Set the desired change type and instruct the system that the rope requires a length change.
	m_CurrentChangeType = Style;
	m_bNeedsChange = value;
}


void UGrappleHookComp::ToggleAutoLengthChange()
{
	// If not attached, safety application to false, and break.
	if (!IsAttached()) { m_bAutoLengthChange = false; return; }

	// else toggle the state of auto taught.
	m_bAutoLengthChange = !m_bAutoLengthChange;

	/* If the user now does not want auto length changes:											  /
	/  - Change the current maximum rope length and bounds using the current length as the low bound */
	if (!m_bAutoLengthChange) {	CalculateBoundsAndSetCurrentMaximum(m_CurrentRopeLength, true); }
	/* Else: The use wants auto length changes so:						  /
	/  - Set the bounds and current maximum back to their initial values  /
	/  - Perform Rope Tighten Logic										 */
	else { 
		CalculateBoundsAndSetCurrentMaximum(m_OverallMaximumRopeLength, false); 

		/* If the rope state is not already taught:  /
		/  - Tighten the rope						*/
		if (m_RopeState != ERopeState::RS_Taught) { BeginRopeTighten(); }
	}

	// Broadcast that the state of auto length change has changed.
	m_AutoLengthChangeDelegate.Broadcast(m_bAutoLengthChange);
}

#pragma endregion


#pragma region Taught Rope Zone

void UGrappleHookComp::ApplyForceTowardsPoint()
{
	// Get the vector to the grapple point.
	FVector ToPoint = GetVectorToGrapplePoint();

	// Add force in its direction scaled appropriately.
	m_MovementComponent_Reference->Velocity += ToPoint.GetSafeNormal() * m_GrappleDangleForce;
}


void UGrappleHookComp::CalculateBoundsAndSetCurrentMaximum(float newCurrentMaximum, bool bCurrentMaxIsLowBound)
{
	// Initially change the current max rope length (perform checks and modifiers below). (Clamp this to At least the bounds offset + a small amount (10))
	m_CurrentMaximumRopeLength = FMath::Max(newCurrentMaximum, FMath::Abs(m_BoundOffset) + 10.f);

	//NOTE: m_BoundOffset is always negative.

	/* If the newly set current maximum rope length is desired to be the low bound:  /
	/  - Clamp the current max to the overall maximum minus the bounds				 /
	/  - Set the Low bound length													 /
	/  - Set the current max back to where it should be.							*/
	if (bCurrentMaxIsLowBound) {
		m_CurrentMaximumRopeLength = FMath::Min(m_OverallMaximumRopeLength + m_BoundOffset, m_CurrentMaximumRopeLength);
		m_LowBoundLength = m_CurrentMaximumRopeLength;
		m_CurrentMaximumRopeLength = m_LowBoundLength - m_BoundOffset;
	}
	/* Else: The function was asked to set the low bound based on the new current maximum:  /
	/  - Perform the simple calculation													   */
	else { m_LowBoundLength = m_CurrentMaximumRopeLength + m_BoundOffset; }
}

#pragma endregion


#pragma region Rope Taught Zone

//TODO: Remove float amount as no longer axis event
void UGrappleHookComp::ApplyRopeTaughtForce()
{
	/* If the player is attached to a grapple point:     /
	/  - Proceed with taught velocity adjustment logic  */
	if (IsAttached()) {

		/* If the player is moving on the ground:		     /
		/  - Proceed with taught velocity adjustment logic  */
		if (m_MovementComponent_Reference->IsMovingOnGround()) {

			// Calculate the velocity and the ToGrapplePoint vectors, normalizing the grapple 
			FVector Velocity = m_MovementComponent_Reference->Velocity;
			FVector ToGrapplePoint = GetVectorToGrapplePoint().GetSafeNormal();

			/* If the angle between the velocity and the direction towards the grapple point is negative (Angle is obtuse):  /
			/  - Proceed with taught velocity adjustment logic																*/
			if (FVector::DotProduct(Velocity, ToGrapplePoint) < SMALL_NUMBER) {

				/* If the player is not within the low bound length of the grapple point:  /
				/  - Apply taught velocity adjustment to the player						  */
				if (m_CurrentAttachmentDistance > m_LowBoundLength) {

					// Calculate a normalized percentage value for the distance the player has transgressed into the bounds.
					float BoundsPercentage = (m_CurrentAttachmentDistance - m_LowBoundLength) / FMath::Abs(m_BoundOffset);

					// The percentage value used to calculate the force.
					float ForceScale;

					// Use the region we are in for the precalculated array to linearlize the force scale..
					if (BoundsPercentage <= 0.25f) { ForceScale = m_TaughtRopeForce[0]; }
					else if (BoundsPercentage > 0.25f && BoundsPercentage <= 0.5f) { ForceScale = m_TaughtRopeForce[1]; }
					else if (BoundsPercentage > 0.5f && BoundsPercentage <= 0.75) { ForceScale = m_TaughtRopeForce[2]; }
					else if (BoundsPercentage > 0.75f) { ForceScale = m_TaughtRopeForce[3]; }

					// Calculate the velocity to apply.
					FVector ForceToApply = ToGrapplePoint * ForceScale * m_TaughtRopeForceApply;

					// Apply the velocity to the player.
					m_MovementComponent_Reference->Velocity += ForceToApply;

				}
			}
		}
	}
}

#pragma endregion


#pragma region Grapple Utility

bool UGrappleHookComp::IsAttached()
{
	if (m_AttachedTo == NULL) { return false; }
	return m_AttachedTo->IsValidLowLevel();
}


void UGrappleHookComp::UpdateRopeState()
{
	if (!IsAttached()) { m_RopeState = ERopeState::RS_Lax; return; }

	// Calculate the current rope state conditions.
	// if Taught
	if (m_CurrentMaximumRopeLength - m_CurrentRopeLength <= 10.0f) { m_RopeState = ERopeState::RS_Taught; }
	// If Lacks
	else if (m_CurrentRopeLength - m_CurrentAttachmentDistance > 10.f ) { m_RopeState = ERopeState::RS_Lax; }
	// Else in between
	else { m_RopeState = ERopeState::RS_Between; }

	

}


void UGrappleHookComp::SetRopeVisible(bool visible)
{
	m_GrappleRope_Reference->SetVisibility(visible);
}


FVector UGrappleHookComp::GetVectorToGrapplePoint()
{
	// If not attached, set outVector to null and return false
	if (!IsAttached()) { return FVector::ZeroVector; }

	// Calculate the vector to the grapple point.
	FVector temp = GetPositionOfGrapplePoint() - GetOwner()->GetActorLocation();

	// Return the vector.
	return temp;
}


FVector UGrappleHookComp::GetPositionOfGrapplePoint()
{
	// If not attached to a grapple point, return a zero vector.
	if (!this->IsAttached()) { return FVector::ZeroVector; }

	// TODO: Inefficient
	// Store the new position of the grapple point.
	UStaticMeshComponent* TempStaticMesh = Cast<UStaticMeshComponent>(m_AttachedTo->GetOwner()->GetComponentByClass(UStaticMeshComponent::StaticClass()));
	m_GrapplePointPos = TempStaticMesh->GetSocketLocation("GrappleSocket");

	// else return the position of the attached to position.
	return m_GrapplePointPos;
}

#pragma endregion


#pragma region Deprecated

void UGrappleHookComp::BeginPlayRopeAttach_DEPRECATED(UVDRopeComponent* GrappleRope)
{
	FVDParticleAttachment temp;
	temp.ComponentRef.OtherActor = GetOwner();
	temp.ParticleIndex = 0;
	GrappleRope->bSnapToSimulatingAttachment = true;
	GrappleRope->ParticleAttachments.Add(temp);
}

#pragma endregion